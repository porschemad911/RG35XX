## RG35XX Stuff and Things

You'll find an array of tools, patches, modifications for the RG35XX handheld console.

I'll try my best to keep this up-to-date as best as I can.

You can also find most of these on the Super Archive (https://archive.xonglebongle.com)

Enjoy!
