#include <stdio.h>
#include <stdlib.h>

void base64_encode(const unsigned char *input, size_t length, FILE *output) {
    const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    for (size_t i = 0; i < length; i += 3) {
        unsigned char a = input[i];
        unsigned char b = (i + 1 < length) ? input[i + 1] : 0;
        unsigned char c = (i + 2 < length) ? input[i + 2] : 0;

        unsigned char output_chars[4];
        output_chars[0] = base64_table[a >> 2];
        output_chars[1] = base64_table[((a & 0x03) << 4) | (b >> 4)];
        output_chars[2] = base64_table[((b & 0x0F) << 2) | (c >> 6)];
        output_chars[3] = base64_table[c & 0x3F];

        for (int j = 0; j < 4; j++) {
            fputc(output_chars[j], output);
        }
    }

    size_t padding = 3 - (length % 3);
    while (padding > 0 && padding < 3) {
        fputc('=', output);
        padding--;
    }
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("usage: %s <input> <output>\n", argv[0]);
        return 1;
    }

    const char *input_name = argv[1];
    const char *output_name = argv[2];

    FILE *input = fopen(input_name, "rb");
    if (!input) {
        perror("Error opening input file");
        return 1;
    }

    fseek(input, 0, SEEK_END);
    long input_size = ftell(input);
    fseek(input, 0, SEEK_SET);

    unsigned char *data = (unsigned char *)malloc(input_size);
    fread(data, 1, input_size, input);

    fclose(input);

    FILE *output = fopen(output_name, "wb");
    if (!output) {
        perror("Error opening output file");
        free(data);
        return 1;
    }

    base64_encode(data, input_size, output);

    fclose(output);
    free(data);

    return 0;
}