#!/bin/bash

export XHOST=arm-rg35xx-linux-uclibcgnueabi
export XTOOL=$HOME/x-tools/$XHOST

export PATH="${PATH}:$XTOOL/bin"

export SYSROOT=$XTOOL/$XHOST/sysroot
export DESTDIR=$SYSROOT

export CC=$XTOOL/bin/$XHOST-gcc
export CXX=$XTOOL/bin/$XHOST-c++
export AR=$XTOOL/bin/$XHOST-ar
export LD=$XTOOL/bin/$XHOST-ld
export STRIP=$XTOOL/bin/$XHOST-strip

export LD_LIBRARY_PATH="$SYSROOT/usr/lib"
export CPP_FLAGS="--sysroot=$SYSROOT -I$SYSROOT/usr/include -I$SYSROOT/usr/local/include -I$SYSROOT/usr/include/SDL -I$SYSROOT/usr/include/freetype2"
export LD_FLAGS="-L$SYSROOT -L$SYSROOT/lib -L$SYSROOT/usr -L$SYSROOT/usr/lib -L$SYSROOT/usr/include -L$SYSROOT/usr/local/lib"

export CPPFLAGS=$CPP_FLAGS
export LDFLAGS=$LD_FLAGS

export CFLAGS=$CPP_FLAGS
export CCFLAGS=$CPP_FLAGS
export CXXFLAGS=$CPP_FLAGS

export INC_DIR=$CPP_FLAGS
export LIB_DIR=$LD_FLAGS

export ARMABI=$XHOST
export TOOLCHAIN_DIR=$HOME/x-tools/$ARMABI

export SDL_CONFIG=$TOOLCHAIN_DIR/$ARMABI/sysroot/usr/bin/sdl-config
export FREETYPE_CONFIG=$TOOLCHAIN_DIR/$ARMABI/sysroot/usr/bin/freetype-config

export CROSS_PREFIX=$XTOOL/bin/$XHOST-
export CROSS_COMPILE=$XTOOL/bin/$XHOST-
