#!/bin/sh

if [ -e checksum ]; then
  rm checksum
fi

touch checksum

crc_set=""

for lpl_file in *.lpl; do
  if [ -e "$lpl_file" ]; then
    db_name="${lpl_file%.lpl}"

    if [ -e "$db_name".rdb ]; then
      rm "$db_name".rdb
    fi

    jq -r '.items[] | "\ngame (\n    title \"\(.label)\"\n    rom ( name \"\(.path | split("/")[-1])\" crc \(.crc32 | split("|")[0]) )\n)"' "$lpl_file" |
    {
      echo "clrmamepro ("
      echo "    name \"$db_name\""
      echo "    description \"muOS Custom Database\""
      echo "    version \"2023.9.4\""
      echo ")"
      cat -
    } >"$db_name".dat

    while read -r crc; do
      if [ -n "$crc" ]; then
        if echo "$crc_set" | grep -q "$crc"; then
          printf "Duplicate CRC32 Checksums Found:\n\tDB: %s\n\tCRC32: %s\n\n" "$db_name" "$crc"
          jq -r --arg crc "$crc" '.items[] | select(.crc32 | split("|")[0] == $crc) | .label' "$lpl_file" | while read -r line; do printf "\t\t%s\n" "$line"; done
          printf "\nPlease fix and run $0 again\n"
          rm *.rdb *.dat checksum 2>&1 >/dev/null
          exit 1
        fi
        crc_set="$crc_set$crc "
      fi
    done < <(jq -r '.items[] | .crc32 | split("|")[0]' "$lpl_file")

    jq -r '.items[] | .crc32 | split("|")[0]' "$lpl_file" >> checksum
    ./dat2rdb "$db_name".rdb "$db_name".dat 2>&1 >/dev/null

    rm "$db_name".dat
  fi
done
