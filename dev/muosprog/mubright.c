#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include "common.h"
#include "options.h"

#define JOY_HOTKEY JOY_SELECT
#define MAX_EVENTS 4

int js_fd;
int js_hk;
int hotkey = 0;

int read_brightness() {
    FILE* file = fopen(BL_BRIGHT_FILE, "r");
    if (file != NULL) {
        int brightness;
        fscanf(file, "%d", &brightness);
        fclose(file);
        return brightness;
    } else {
        perror("Failed to open brightness file");
        return 192;
    }
}

void set_brightness(int brightness) {
    FILE* file = fopen(BL_BRIGHT_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", brightness);
        fclose(file);
    } else {
        perror("Failed to open brightness file");
    }
}

void save_brightness(int brightness) {
    FILE* file = fopen(BL_RST_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", brightness);
        fclose(file);
    } else {
        perror("Failed to open brightness restore file");
        exit(1);
    }
}

void read_joystick_events() {
    struct js_event ev;
    int    b_new = -1;

    int b_current = read_brightness();

    if (b_new == -1)
        b_new = b_current;

    while (read(js_fd, &ev, sizeof(struct js_event)) > 0) {
        if (ev.type == JS_EVENT_BUTTON && ev.value == 1 && hotkey == 1) {
            if (ev.number == JOY_PLUS) {
                b_new = b_current + BL_INC;
                b_new = (b_new > BL_MAX) ? BL_MAX : b_new;
            } else if (ev.number == JOY_MINUS) {
                b_new = b_current - BL_INC;
                b_new = (b_new < BL_MIN) ? BL_MIN : b_new;
            }
        }
    }

    if (hotkey == 1 && b_new != b_current) {
        set_brightness(b_new);
        save_brightness(b_new);
        set_bl_power(b_new == 0 ? 1 : 0);
    }
}

void open_joystick_device() {
    js_fd = open(JOY_DEVICE, O_RDONLY | O_NONBLOCK);
    js_hk = open(JOY_DEVICE, O_RDONLY | O_NONBLOCK);

    if (js_fd == -1 || js_hk == -1) {
        perror("Failed to open joystick device");
        exit(1);
    }
}

void* restore_brightness_task(void* arg) {
    int b_default = BL_DEF;
    int b_file;

    while (1) {
        FILE *file = fopen(BL_RST_FILE, "r");
        if (file != NULL) {
            fscanf(file, "%d", &b_file);
            fclose(file);
            if (b_file == 0)
                b_file = b_default;
            else
                b_file = (b_file > BL_MAX) ? BL_MAX : b_file;
            set_brightness(b_file);
        } else {
            set_brightness(b_default);
        }

        usleep(250000);
    }

    return NULL;
}

void* get_hotkey_task(void* arg) {
    struct js_event ev;

    while (1) {
        while (read(js_hk, &ev, sizeof(struct js_event)) > 0) {
            if (ev.type == JS_EVENT_BUTTON && ev.number == JOY_HOTKEY) {
                hotkey = ev.value == 1 ? 1 : 0;
            }
        }

        usleep(250000);
    }

    return NULL;
}

int main() {
    setup_background_process();
    open_joystick_device();

    pthread_t restore_brightness_thread;
    if (pthread_create(&restore_brightness_thread, NULL, restore_brightness_task, NULL) != 0) {
        perror("Failed to create restore brightness thread");
        exit(1);
    }

    pthread_t get_hotkey_thread;
    if (pthread_create(&get_hotkey_thread, NULL, get_hotkey_task, NULL) != 0) {
        perror("Failed to create get hotkey thread");
        exit(1);
    }

    int epoll_fd = epoll_init(js_fd);
    struct epoll_event events[MAX_EVENTS];

    while (1) {
        int num_events = epoll_wait_events(epoll_fd, events, MAX_EVENTS);

        for (int i = 0; i < num_events; ++i) {
            if (events[i].data.fd == js_fd && (events[i].events & EPOLLIN)) {
                read_joystick_events();
            }
        }
    }

    pthread_join(restore_brightness_thread, NULL);
    pthread_join(get_hotkey_thread, NULL);
    close(epoll_fd);

    return 0;
}
