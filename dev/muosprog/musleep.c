#include <linux/input.h>
#include <dirent.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <linux/joystick.h>
#include <sys/ioctl.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <time.h>
#include "common.h"
#include "options.h"

#define JOY_HOTKEY JOY_PLUS
#define MAX_EVENTS 4

int js_fd;
int js_hk;
int hotkey = 0;
int is_sleep = 0;
int prev_volume;
int idle_timeout;
int idle_shutdown;

time_t time_input;
time_t time_start;

pid_t find_pid_by_command(const char *command) {
    DIR *dir;
    struct dirent *entry;
    pid_t pid = -1;

    dir = opendir("/proc");
    if (dir == NULL) {
        perror("opendir");
        return -1;
    }

    char cmdline[64];
    while ((entry = readdir(dir))) {
        if (isdigit(entry->d_name[0])) {
            char cmdPath[64];
            snprintf(cmdPath, sizeof(cmdPath), "/proc/%s/cmdline", entry->d_name);

            FILE *fp = fopen(cmdPath, "r");
            if (fp) {
                if (fgets(cmdline, sizeof(cmdline), fp)) {
                    char *firstSpace = strchr(cmdline, ' ');
                    if (firstSpace) {
                        *firstSpace = '\0';
                    }
                    if (strcmp(cmdline, command) == 0) {
                        pid = atoi(entry->d_name);
                        break;
                    }
                }
                fclose(fp);
            }
        }
    }

    closedir(dir);
    return pid;
}

void control_process(const char *command, int signal) {
    pid_t pid = find_pid_by_command(command);
    
    if (pid != -1) {
        kill(pid, signal);
    }
}

void process_exec(int signal) {
    control_process(MUAUDIO_EXEC, signal);
    control_process(MUBRIGHT_EXEC, signal);
    control_process(MUSCREEN_EXEC, signal);
    control_process(MUWATCH_EXEC, signal);
    control_process(RETROARCH_EXEC, signal);
}

void mode_sleep() {
    is_sleep = 1;
    set_bl_power(1);
    prev_volume = get_volume(VOL_SPK_MASTER);
    set_volume(VOL_SPK_MASTER, 0);
    set_governor("powersave");
    set_cpu_scale(288000);
    process_exec(SIGSTOP);
}

void mode_wake() {
    is_sleep = 0;
    set_bl_power(0);
    set_volume(VOL_SPK_MASTER, prev_volume);
    set_governor("userspace");
    set_cpu_scale(1488000);
    process_exec(SIGCONT);
}

void read_joystick_events() {
    struct js_event ev;

    while (read(js_fd, &ev, sizeof(struct js_event)) > 0) {      
        time_start = time(NULL);
        if (ev.type == JS_EVENT_BUTTON && ev.number == JOY_POWER && ev.value == 1 && hotkey == 0) {
            if (find_pid_by_command(MUOSUTIL_EXEC) == -1) {
                is_sleep ? mode_wake() : mode_sleep();
            }
        }
    }
}

void open_joystick_device() {
    js_fd = open(JOY_DEVICE, O_RDONLY | O_NONBLOCK);
    js_hk = open(JOY_DEVICE, O_RDONLY | O_NONBLOCK);

    if (js_fd == -1 || js_hk == -1) {
        perror("Failed to open joystick device");
        exit(1);
    }
}

void* get_hotkey_task(void* arg) {
    struct js_event ev;

    while (1) {
        while (read(js_hk, &ev, sizeof(struct js_event)) > 0) {
            if (ev.type == JS_EVENT_BUTTON && ev.number == JOY_HOTKEY) {
                hotkey = ev.value == 1 ? 1 : 0;
            }
        }

        usleep(250000);
    }

    return NULL;
}

void* get_monitor_task(void* arg) {
    while (1) {
        time_t time_current = time(NULL) - time_start;
        time_input = time_current;

        if (time_input > idle_timeout && is_sleep == 0) {
            mode_sleep();
        }

        if (time_input > idle_shutdown && is_sleep == 1) {
            // TODO: Has to be a better way to do this!
            system("sync && busybox poweroff -f");
        }

        sleep(1);
    }

    return NULL;
}

int main(int argc, char *argv[]) {
    idle_timeout = ID_SHUTDOWN;
    idle_shutdown = ID_TIMEOUT;

    if (argc > 2) {
        idle_timeout = atoi(argv[1]);
        idle_shutdown = atoi(argv[2]);
    }

    setup_background_process();
    open_joystick_device();

    pthread_t get_hotkey_thread;
    if (pthread_create(&get_hotkey_thread, NULL, get_hotkey_task, NULL) != 0) {
        perror("Failed to create get hotkey thread");
        exit(1);
    }

    time_start = time(NULL);

    pthread_t get_monitor_thread;
    if (pthread_create(&get_monitor_thread, NULL, get_monitor_task, NULL) != 0) {
        perror("Failed to create get monitor thread");
        exit(1);
    }

    int epoll_fd = epoll_init(js_fd);
    struct epoll_event events[MAX_EVENTS];

    while (1) {
        int num_events = epoll_wait_events(epoll_fd, events, MAX_EVENTS);

        for (int i = 0; i < num_events; ++i) {
            if (events[i].data.fd == js_fd && (events[i].events & EPOLLIN)) {
                read_joystick_events();
            }
        }
    }

    pthread_join(get_hotkey_thread, NULL);
    pthread_join(get_monitor_thread, NULL);
    close(epoll_fd);

    return 0;
}
