#include "lvgl/lvgl.h"
#include "lv_drivers/display/fbdev.h"
#include "lv_drivers/indev/evdev.h"
#include "lvgl/src/core/lv_event.h"
#include "lvgl/src/core/lv_group.h"
#include "lvgl/src/core/lv_obj.h"
#include "lvgl/src/misc/lv_anim.h"
#include "lvgl/src/misc/lv_style.h"
#include "lvgl/src/widgets/lv_label.h"
#include "lvgl/src/widgets/lv_roller.h"
#include "lvgl/src/widgets/lv_textarea.h"
#include "ui.h"
#include <regex.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <sys/mount.h>
#include <sys/reboot.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/vfs.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include <linux/rtc.h>
#include <zip.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define JOY_A		0
#define JOY_B		1
#define JOY_X		2
#define JOY_Y		3
#define JOY_POWER	4
#define JOY_L1		5
#define JOY_R1		6
#define JOY_SELECT	7
#define JOY_START	8
#define JOY_MENU	9
#define JOY_PLUS	10
#define JOY_MINUS	11

#define JOY_UP		7
#define JOY_DOWN	7
#define JOY_LEFT	6
#define JOY_RIGHT	6
#define JOY_L2		2
#define JOY_R2		5

#define MAX_BUFFER_SIZE 256
#define DISP_BUF_SIZE	(640 * 480 * 32) / 8

#define MISC_MOUNTPOINT "/misc"
#define MUOS_EXTRA_FILE "/misc/options.txt"
#define MUOS_EXTRA_TEMP "/tmp/options.txt"

#define GOVERNOR_FILE  "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"
#define SCALE_MN_FILE  "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"

#define UI_BAR_COLOR        lv_color_hex(0x403A03)
#define UI_BAR_BORDER_COLOR lv_color_hex(0x807506)
#define UI_BAR_PADDING      2

static int js_fd;
static lv_obj_t* active_screen = NULL;

int debug = 0;
int turbo_mode = 0;
int msgbox_active = 0;
int input_disable = 0;
int tf2_found = 0;
int reset_method = 0;
int input_test_right = 0;
int input_test_down = 0;
int tracker_scroll = 0;

int rolYear;
int rolMonth;
int rolDay;
int rolHour;
int rolMinute;

lv_obj_t* msgbox_element = NULL;

typedef struct {
    char* name;
    char* value;
    lv_obj_t* widget;
} Option;

typedef struct {
    const char *name;
    const char *value;
} OptionSend;

Option* options = NULL;
int num_options = 0;

void text_replace(char *str, const char *find, const char *replace) {
    char *pos = strstr(str, find);
    if (pos != NULL) {
        size_t findLen = strlen(find);
        size_t replaceLen = strlen(replace);

        memmove(pos + replaceLen, pos + findLen, strlen(pos + findLen) + 1);
        memcpy(pos, replace, replaceLen);
    }
}

char* get_execute_result(const char *command) {
    FILE *fp = popen(command, "r");
    if (fp == NULL) {
        fprintf(stderr, "Failed to run command: %s\n", command);
        return NULL;
    }

    static char result[MAX_BUFFER_SIZE];
    fgets(result, MAX_BUFFER_SIZE, fp);
    pclose(fp);

    char *newline = strchr(result, '\n');
    if (newline != NULL)
        *newline = '\0';

    return result;
}

void set_governor(char* governor) {
    FILE* file = fopen(GOVERNOR_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%s", governor);
        fclose(file);
    } else {
        perror("Failed to open scaling_governor file");
        exit(1);
    }
}

void set_cpu_scale(int speed) {
    FILE* file = fopen(SCALE_MN_FILE, "w");
    if (file != NULL) {
        fprintf(file, "%d", speed);
        fclose(file);
    } else {
        perror("Failed to open scaling_max_freq file");
        exit(1);
    }
}

int compare(const void* a, const void* b) {
    return strcmp(*(const char**)a, *(const char**)b);
}

char* remove_comma(const char *str) {
    size_t len = strlen(str);
    char *result = malloc(len + 1);

    if (result == NULL) {
        fprintf(stderr, "Failed to allocate memory\n");
        return NULL;
    }

    strcpy(result, str);
    if (len > 0 && result[len - 1] == ',')
        result[len - 1] = '\0';

    return result;
}

char* format_uptime(const char *uptime) {
    int hours = 0, minutes = 0;

    if (sscanf(uptime, "%d:%d", &hours, &minutes) < 2) {
        sscanf(uptime, "%d", &minutes);
        minutes = hours;
        hours = 0;
    }

    static char formattedUptime[MAX_BUFFER_SIZE];

    if (hours > 0) {
        sprintf(formattedUptime, "%d %s %d %s",
                hours, (hours == 1) ? "Hour" : "Hours",
                minutes, (minutes == 1) ? "Minute" : "Minutes");
    } else {
        sprintf(formattedUptime, "%d %s",
                minutes, (minutes == 1) ? "Minute" : "Minutes");
    }

    return formattedUptime;
}

void update_system_info() {
    char sysInfoMessage[MAX_BUFFER_SIZE * 2];
    strcpy(sysInfoMessage, lv_textarea_get_text(ui_txtSysInfoMessage));

    text_replace(sysInfoMessage, "%muosver%", get_execute_result("busybox cat /etc/release"));
    text_replace(sysInfoMessage, "%linver%", get_execute_result("busybox uname -srm"));
    text_replace(sysInfoMessage, "%uptime%", format_uptime(remove_comma(get_execute_result("busybox uptime | busybox awk '{print $3}'"))));
    text_replace(sysInfoMessage, "%cpuinfo%", get_execute_result("busybox cat /proc/cpuinfo | busybox grep 'model name' | busybox head -n1 | busybox cut -d':' -f2 | busybox awk '{$1=$1};1'"));
    text_replace(sysInfoMessage, "%cpuspeed%", get_execute_result("busybox cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq | busybox awk '{print $1/1000}'"));
    text_replace(sysInfoMessage, "%cpugov%", get_execute_result("busybox cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"));
    text_replace(sysInfoMessage, "%ram%", get_execute_result("busybox free | busybox awk '/Total:/ {used = $3 / 1024; total = $2 / 1024; printf \"%.2f MB / %.2f MB\", used, total}'"));
    text_replace(sysInfoMessage, "%temp%", get_execute_result("busybox cat /sys/class/thermal/thermal_zone1/temp | busybox awk '{print $1/1000}'"));
    text_replace(sysInfoMessage, "%runserv%", get_execute_result("busybox ps | busybox grep -v 'COMMAND' | busybox grep -v 'grep' | busybox sed '/\\[/d' | busybox wc -l"));
    text_replace(sysInfoMessage, "%load%", get_execute_result("busybox uptime | busybox awk -F': ' '{print $NF}'"));

    lv_textarea_set_text(ui_txtSysInfoMessage, sysInfoMessage);
}

const char* fat_filesystem(const char* device_file) {
    int fd = open(device_file, O_RDONLY);
    if (fd == -1) {
        perror("Error opening device file");
        return "unknown";
    }

    struct statfs fs;
    if (fstatfs(fd, &fs) == -1) {
        close(fd);
        perror("Error getting file system information");
        return "unknown";
    }

    close(fd);

    if (fs.f_type == 16914836) {
        return "vfat";
    } else {
        return "unknown";
    }
}

void run_fsck_health_check(const char *device_path) {
    char fs_device[MAX_BUFFER_SIZE];
    snprintf(fs_device, sizeof(fs_device), "/dev/block/mmcblk%s", device_path);

    const char* filesystem_type = fat_filesystem(fs_device);

    if (strcmp(filesystem_type, "unknown") == 0) {
        lv_textarea_add_text(ui_txtCardToolsInfo, "Filesystem is not of type FAT - Skipping...\n");
        return;
    }

    char fsck[MAX_BUFFER_SIZE];
    snprintf(fsck, sizeof(fsck), "/sbin/fsck.vfat -aVw /dev/block/mmcblk%s", device_path);

    char dev_errmsg[MAX_BUFFER_SIZE];
    snprintf(dev_errmsg, sizeof(dev_errmsg), "Could not check /dev/block/mmcblk%s\n", device_path);

    FILE* fp = popen(fsck, "r");
    if (!fp) {
        lv_textarea_add_text(ui_txtCardToolsInfo, dev_errmsg);
        return;
    }

    char buffer[MAX_BUFFER_SIZE];
    while (fgets(buffer, sizeof(buffer), fp) != NULL) {
        lv_textarea_add_text(ui_txtCardToolsInfo, buffer);
    }

    pclose(fp);
}

int detect_TF2_card() {
    FILE *fp;
    char line[MAX_BUFFER_SIZE];
    const char *target = "mmcblk1";
    int found = 0;

    fp = fopen("/proc/partitions", "r");
    if (!fp) {
        perror("Error opening /proc/partitions");
        return 0;
    }

    while (fgets(line, sizeof(line), fp)) {
        if (strstr(line, target)) {
            found = 1;
            break;
        }
    }

    fclose(fp);

    if (found) {
        tf2_found = 1;
        return 1;
    } else {
        return 0;
    }
}

void format_sdcard(const char* fs_type, const char* fs_code) {
    input_disable = 1;
    lv_textarea_set_text(ui_txtCardToolsInfo, "");

    char command[MAX_BUFFER_SIZE];
    snprintf(command, sizeof(command), "umount /dev/block/mmcblk1; echo 'type=%s' | /sbin/sfdisk /dev/block/mmcblk1 --wipe always; /sbin/mkfs.%s -F /dev/block/mmcblk1", fs_code, fs_type);

    FILE* fp = popen(command, "r");
    if (!fp) {
        perror("Failed to execute format command");
    }

    char output[MAX_BUFFER_SIZE];
    while (fgets(output, sizeof(output), fp) != NULL) {
        lv_textarea_add_text(ui_txtCardToolsInfo, output);
    }

    pclose(fp);
    input_disable = 0;
}

void execute_sync(const char* source, const char* destination, lv_obj_t* element) {
    char command[MAX_BUFFER_SIZE];
    snprintf(command, sizeof(command), "busybox cp -af %s/* %s/", source, destination);

    FILE* fp = popen(command, "r");
    if (!fp) {
        lv_textarea_add_text(element, "Error executing copy command!");
        return;
    }

    char output[MAX_BUFFER_SIZE];
    while (fgets(output, sizeof(output), fp) != NULL) {
        lv_textarea_add_text(element, output);
    }

    pclose(fp);
}

void progress_callback_backup(int progress) {
    lv_bar_set_value(ui_barBackupProgress, progress, LV_ANIM_ON);
    
    char progressStr[MAX_BUFFER_SIZE];
    sprintf(progressStr, "%d", progress);
    lv_label_set_text(ui_lblBackupProgress, progressStr);

    if (progress == 100) {
        lv_label_set_text(ui_lblBackupProgress, "Backup Complete");
        input_disable = 0;
    }
}

void extract_archive(const char* archiveName, const char* destinationDir) {
    chdir(destinationDir);

    char command[MAX_BUFFER_SIZE];
    snprintf(command, sizeof(command), "/system/bin/7za x %s -y -aos", archiveName);

    lv_obj_set_style_bg_color(ui_barExtractProgress, lv_color_hex(0xF8E008), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_barExtractProgress, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    input_disable = 1;
    turbo_mode = 1;

    char current_extract[MAX_BUFFER_SIZE];
    char *archiveNameNoDrive = strstr(archiveName, "/mnt/mmc/");
    strcpy(current_extract, "Extracting ");
    strcat(current_extract, archiveNameNoDrive + 9);  // Skip "/mnt/mmc/" (9 characters)
    lv_label_set_text(ui_lblExtractProgress, current_extract);

    int status = system(command);

    turbo_mode = 0;
    input_disable = 0;

    lv_obj_set_style_bg_color(ui_barExtractProgress, lv_color_hex(0x403A03), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_barExtractProgress, 0, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    if (status == -1) {
        lv_label_set_text(ui_lblExtractProgress, "Failed to run extract command!");
    } else if (status == 0) {
        lv_label_set_text(ui_lblExtractProgress, "Extraction complete!");
    } else {
        lv_label_set_text(ui_lblExtractProgress, "Extraction failed!");
    }
}

int create_archive(const char* archiveName, const char* files[], int numFiles) {
    char command[MAX_BUFFER_SIZE];
    snprintf(command, sizeof(command), "/system/bin/7za a -spf -bso0 -bse0 -bsp1 %s", archiveName);

    turbo_mode = 1;
    input_disable = 1;

    for (int i = 0; i < numFiles; i++) {
        strcat(command, " ");
        strcat(command, files[i]);
    }

    FILE* fp = popen(command, "r");
    if (fp == NULL) {
        perror("Error executing command");
        turbo_mode = 0;
        input_disable = 1;
        return 1;
    }

    regex_t regex;
    if (regcomp(&regex, "\\b([0-9]{1,3})\\b", REG_EXTENDED) != 0) {
        perror("Error compiling regex");
        pclose(fp);
        turbo_mode = 0;
        input_disable = 1;
        return 1;
    }

    int maxProgress = 0;
    char buffer[MAX_BUFFER_SIZE];
    while (fgets(buffer, sizeof(buffer), fp) != NULL) {
        regmatch_t matches[2];
        if (regexec(&regex, buffer, 2, matches, 0) == 0) {
            int start = matches[1].rm_so;
            int end = matches[1].rm_eo;
            char number[MAX_BUFFER_SIZE];
            strncpy(number, buffer + start, end - start);
            number[end - start] = '\0';
            int value = atoi(number);

            if (value > maxProgress) {
                maxProgress = value;
                progress_callback_backup(maxProgress);
            }

            if (maxProgress >= 100) {
                break;
            }
        }
    }

    regfree(&regex);
    pclose(fp);

    turbo_mode = 0;
    input_disable = 1;

    return 0;
}

void cleanup_compressed_files(const char* files[], int numFiles) {
    for (int i = 0; i < numFiles; i++) {
        free((void*)files[i]);
    }
}

static void dropdown_event_handler(lv_event_t* e) {
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);

    if (code == LV_EVENT_VALUE_CHANGED) {
        char buf[MAX_BUFFER_SIZE];
        lv_dropdown_get_selected_str(obj, buf, sizeof(buf));
        if (active_screen == ui_scrReset) {
            switch (lv_dropdown_get_option_index(ui_droReset, buf)) {
                case -1:
                    reset_method = -1;
                    break;
                case 1:
                    reset_method = 1;
                    break;
            }
        }
    }
}

const char* map_text(const char* element_text) {
    if (strcmp(element_text, "Silent Boot") == 0) {
        return "SILENT";
    } else if (strcmp(element_text, "Gamepad Wait") == 0) {
        return "GAMEPAD";
    } else if (strcmp(element_text, "Enable ADB") == 0) {
        return "ADB";
    } else if (strcmp(element_text, "Enable HDMI") == 0) {
        return "HDMI";
    }

    return "UNKNOWN";
}

static void checkbox_event_handler(lv_event_t* e) {
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);

    if (code == LV_EVENT_VALUE_CHANGED) {
        const char* txt = lv_checkbox_get_text(obj);
        const char* state = lv_obj_get_state(obj) & LV_STATE_CHECKED ? "true" : "false";
        const char* option_variable = map_text(txt);
    }
}

static void roller_event_handler(lv_event_t* e) {
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);

    if (code == LV_EVENT_VALUE_CHANGED && (obj == ui_rolMonth || obj == ui_rolYear)) {
        char buf[MAX_BUFFER_SIZE];
        char year[MAX_BUFFER_SIZE];
        char month[MAX_BUFFER_SIZE];

        lv_roller_get_selected_str(obj, buf, sizeof(buf));
        lv_roller_get_selected_str(ui_rolYear,  year, sizeof(year));
        lv_roller_get_selected_str(ui_rolMonth, month, sizeof(month));

        int month_days = 0;
        if (strcmp(buf, "January")   == 0 ||
            strcmp(buf, "March")     == 0 ||
            strcmp(buf, "May")       == 0 ||
            strcmp(buf, "July")      == 0 ||
            strcmp(buf, "August")    == 0 ||
            strcmp(buf, "October")   == 0 ||
            strcmp(buf, "December")  == 0) {
                month_days = 31;
        } else if (strcmp(buf, "April") == 0 ||
            strcmp(buf, "June")         == 0 ||
            strcmp(buf, "September")    == 0 ||
            strcmp(buf, "November")     == 0) {
                month_days = 30;
        }

        int iyear = atoi(year);
        if ((iyear % 4 == 0 && iyear % 100 != 0) || iyear % 400 == 0) {
            if (strcmp(month, "February") == 0)
                month_days = 29;
        } else {
            if (strcmp(month, "February") == 0)
                month_days = 28;
        }

        if (month_days > 0) {
            char day_string[MAX_BUFFER_SIZE];
            day_string[0] = '\0';

            for (int i = 1; i <= month_days; i++) {
                char day[MAX_BUFFER_SIZE];
                sprintf(day, "%d", i);
                strcat(day_string, day);
                if (i != month_days)
                    strcat(day_string, "\n");
            }

            lv_roller_set_options(ui_rolDay, day_string, LV_ROLLER_MODE_NORMAL);
            lv_roller_set_selected(ui_rolDay, lv_roller_get_selected(ui_rolDay), LV_ANIM_OFF);

        }
    }
}

void elements_events_init() {
    lv_obj_add_event_cb(ui_droTheme, dropdown_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_droBattery, dropdown_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_droLowBattery, dropdown_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_droBlank, dropdown_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_droShutdown, dropdown_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_droReset, dropdown_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_droSetupBattery, dropdown_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_droSetupCard, dropdown_event_handler, LV_EVENT_ALL, NULL);

    lv_obj_add_event_cb(ui_chkSilent, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkHDMI, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkGamepad, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkDebug, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkConfigs, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkPlaylists, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkSaves, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkSystem, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkWinCompat, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkSilentSetup, checkbox_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_chkDebugSetup, checkbox_event_handler, LV_EVENT_ALL, NULL);

    lv_obj_add_event_cb(ui_rolYear, roller_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_rolMonth, roller_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_rolDay, roller_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_rolHour, roller_event_handler, LV_EVENT_ALL, NULL);
    lv_obj_add_event_cb(ui_rolMinute, roller_event_handler, LV_EVENT_ALL, NULL);
}

void dropdown_element_change(lv_obj_t* dropdown_element, int item) {
    lv_dropdown_set_selected(dropdown_element, lv_dropdown_get_selected(dropdown_element) + item);
    lv_dropdown_set_selected_highlight(dropdown_element, true);
    lv_dropdown_open(dropdown_element);
}

void roller_element_change(lv_obj_t* roller_element, int item) {
    lv_roller_set_selected(roller_element, lv_roller_get_selected(roller_element) + item, LV_ANIM_OFF);
}

int get_month_number(const char* month) {
    const char* months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    int i;

    for (i = 0; i < 12; i++) {
        if (strncmp(month, months[i], 3) == 0)
            return i;
    }

    return -1;
}

void run_shell_script(const char* shell_script ) {
    if (shell_script) system(shell_script);
}

int load_config(Option** options, int* num_options) {
    FILE* file = fopen(MUOS_EXTRA_FILE, "r");
    if (file == NULL) {
        perror("Failed to open file");
        return -1;
    }

    char line[BUFSIZ];
    int option_count = 0;
    Option* opts = NULL;

    while (fgets(line, sizeof(line), file) != NULL) {
        if (line[0] == '#' || line[0] == '\n') {
            continue;
        }

        char* separator = strchr(line, '=');
        if (separator == NULL) {
            perror("Invalid line");
            continue;
        }

        *separator = '\0';

        Option* new_opt = (Option*)realloc(opts, (option_count + 1) * sizeof(Option));
        if (new_opt == NULL) {
            perror("Memory allocation failed");
            fclose(file);
            free(opts);
            return -1;
        }

        opts = new_opt;

        opts[option_count].name = strdup(line);
        opts[option_count].value = strdup(separator + 1);

        if (opts[option_count].name == NULL || opts[option_count].value == NULL) {
            perror("Memory allocation failed");
            fclose(file);
            free(opts);
            return -1;
        }

        option_count++;
    }

    *options = opts;
    *num_options = option_count;
    fclose(file);
    return 0;
}

char* get_option_value(const Option* options, int num_options, const char* name) {
    for (int i = 0; i < num_options; i++) {
        if (strcmp(options[i].name, name) == 0) {
            char* value = options[i].value;
            int len = strlen(value);

            if (len > 0 && value[len - 1] == '\n') {
                value[len - 1] = '\0';
            }

            return value;
        }
    }

    return NULL;
}

void set_option_values(const OptionSend *option_array, size_t length) {
    FILE *options_file;
    FILE *temp_file;
    char *line = NULL;
    size_t lineSize = 0;
    ssize_t bytesRead;

    options_file = fopen(MUOS_EXTRA_FILE, "r");
    if (options_file == NULL) {
        perror("Error opening options file");
        return;
    }

    temp_file = fopen(MUOS_EXTRA_TEMP, "w");
    if (temp_file == NULL) {
        fclose(options_file);
        perror("Error creating temporary file");
        return;
    }

    while ((bytesRead = getline(&line, &lineSize, options_file)) != -1) {
        int option_found = 0;
        for (size_t i = 0; i < length; ++i) {
            const OptionSend *option_pair = &option_array[i];
            if (strncmp(line, option_pair->name, strlen(option_pair->name)) == 0) {
                fprintf(temp_file, "%s=%s\n", option_pair->name, option_pair->value);
                option_found = 1;
                break;
            }
        }
        if (!option_found) {
            fputs(line, temp_file);
        }
    }

    run_shell_script("/system/data/update.sh");

    fclose(options_file);
    fclose(temp_file);

    options_file = fopen(MUOS_EXTRA_FILE, "w");
    if (options_file == NULL) {
        perror("Error opening options file");
        return;
    }

    temp_file = fopen(MUOS_EXTRA_TEMP, "r");
    if (temp_file == NULL) {
        fclose(options_file);
        perror("Error opening temporary file");
        return;
    }

    while ((bytesRead = getline(&line, &lineSize, temp_file)) != -1) {
        fputs(line, options_file);
    }

    fclose(temp_file);
    fclose(options_file);

    free(line);
}

void read_rtc_hardware() {
    int rtc_fd = open("/dev/rtc0", O_RDONLY);
    if (rtc_fd == -1) {
        perror("Failed to open RTC device");
        return;
    }

    struct rtc_time rtc_tm;
    int result = ioctl(rtc_fd, RTC_RD_TIME, &rtc_tm);
    if (result == -1) {
        perror("Failed to read hardware clock");
        close(rtc_fd);
        return;
    }

    rolYear = rtc_tm.tm_year - 101; // Offset from 2001 - first in roller
    rolMonth = rtc_tm.tm_mon;
    rolDay = rtc_tm.tm_mday - 1;
    rolHour = rtc_tm.tm_hour;
    rolMinute = rtc_tm.tm_min;

    close(rtc_fd);
}

void restore_clock_settings() {
    read_rtc_hardware();

    lv_roller_set_selected(ui_rolYear, rolYear, LV_ANIM_OFF);
    lv_roller_set_selected(ui_rolMonth, rolMonth, LV_ANIM_OFF);
    lv_roller_set_selected(ui_rolDay, rolDay, LV_ANIM_OFF);
    lv_roller_set_selected(ui_rolHour, rolHour, LV_ANIM_OFF);
    lv_roller_set_selected(ui_rolMinute, rolMinute, LV_ANIM_OFF);
}

void restore_boot_options() {
    Option options_chk[] = {
        {"SILENT", get_option_value(options, num_options, "SILENT"), ui_chkSilent},
        {"GAMEPAD", get_option_value(options, num_options, "GAMEPAD"), ui_chkGamepad},
        {"ADB", get_option_value(options, num_options, "ADB"), ui_chkDebug},
        {"HDMI", get_option_value(options, num_options, "HDMI"), ui_chkHDMI}
    };

    Option options_dro[] = {
        {"THEME", get_option_value(options, num_options, "THEME"), ui_droTheme},
        {"BATTERY", get_option_value(options, num_options, "BATTERY"), ui_droBattery}
    };

    const int num_options_chk = sizeof(options_chk) / sizeof(options_chk[0]);

    for (int i = 0; i < num_options_chk; i++) {
        if (strcmp(options_chk[i].value, "true") == 0) {
            lv_obj_add_state(options_chk[i].widget, LV_STATE_CHECKED);
        } else {
            lv_obj_clear_state(options_chk[i].widget, LV_STATE_CHECKED);
        }
    }

    const char* dir_path = "/misc/theme/";
    DIR* dir = opendir(dir_path);

    if (dir == NULL) {
        perror("Failed to open theme directory");
        return;
    }

    lv_dropdown_clear_options(ui_droTheme);

    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR && entry->d_name[0] != '.') {
            lv_dropdown_add_option(ui_droTheme, entry->d_name, LV_DROPDOWN_POS_LAST);
        }
    }

    closedir(dir);

    const char* theme_value = get_option_value(options, num_options, "THEME");
    char* options_text_copy = strdup(lv_dropdown_get_options(ui_droTheme));
    char* option = strtok(options_text_copy, "\n");
    int theme_index = -1;
    int current_index = 0;

    while (option != NULL) {
        if (strcmp(theme_value, option) == 0) {
            theme_index = current_index;
            break;
        }
        option = strtok(NULL, "\n");
        current_index++;
    }

    if (theme_index != -1) {
        lv_dropdown_set_selected(ui_droTheme, theme_index);
    }

    free(options_text_copy);

    const char* battery_value = options_dro[1].value;
    int battery_index = 0;

    if (strcmp(battery_value, "2100") == 0) {
        battery_index = 0;
    } else if (strcmp(battery_value, "2600") == 0) {
        battery_index = 1;
    } else if (strcmp(battery_value, "2800") == 0) {
        battery_index = 2;
    } else if (strcmp(battery_value, "3500") == 0) {
        battery_index = 3;
    }

    lv_dropdown_set_selected(ui_droBattery, battery_index);
}

void update_activity_tracker(const char* activity_log) {
    turbo_mode = 1;
    input_disable = 1;

    lv_obj_add_flag(ui_pnlTrackerContent, LV_OBJ_FLAG_HIDDEN);
    lv_obj_clear_flag(ui_pnlTrackerProgress, LV_OBJ_FLAG_HIDDEN);

    lv_obj_set_style_bg_color(ui_barTrackerProgress, lv_color_hex(0xF8E008), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_barTrackerProgress, 255, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    lv_obj_t* lblTrackerDetailRuntimes[] = {
        ui_lblTrackerOneDetailRuntime,
        ui_lblTrackerTwoDetailRuntime,
        ui_lblTrackerThreeDetailRuntime,
        ui_lblTrackerFourDetailRuntime,
        ui_lblTrackerFiveDetailRuntime,
        ui_lblTrackerSixDetailRuntime,
        ui_lblTrackerSevenDetailRuntime,
        ui_lblTrackerEightDetailRuntime,
        ui_lblTrackerNineDetailRuntime,
        ui_lblTrackerTenDetailRuntime
    };

    lv_obj_t* lblTrackerDetailNames[] = {
        ui_lblTrackerOneDetailName,
        ui_lblTrackerTwoDetailName,
        ui_lblTrackerThreeDetailName,
        ui_lblTrackerFourDetailName,
        ui_lblTrackerFiveDetailName,
        ui_lblTrackerSixDetailName,
        ui_lblTrackerSevenDetailName,
        ui_lblTrackerEightDetailName,
        ui_lblTrackerNineDetailName,
        ui_lblTrackerTenDetailName
    };

    FILE* fp;
    char buf[1024];
    const char* activity_update = "for file in /mnt/mmc/LOGS/*.lrtl; do rom=${file##*/}; /system/bin/jq --arg name \"${rom%%.lrtl}\" '.name = $name | .runtime_seconds = (.runtime | capture(\"(?<hours>\\\\d+):(?<minutes>\\\\d+):(?<seconds>\\\\d+)\") | (.hours|tonumber)*3600 + (.minutes|tonumber)*60 + (.seconds|tonumber))' \"$file\"; done | /system/bin/jq -sr 'sort_by(-.runtime_seconds) | map(del(.version, .runtime_seconds)) | ([\"name\",\"runtime\",\"last_played\"] | join(\";\")), (.[] | [.name, .runtime, .last_played[:-1]] | join(\";\")) | . + \"\"' | busybox head -n 11 > /mnt/mmc/LOGS/activity.csv";

    system(activity_update);

    if (access(activity_log, F_OK) == 0) {
        if (access(activity_log, R_OK) != 0) {
            perror("Error: No read permissions for activity log file.\n");
            turbo_mode = 0;
            input_disable = 0;
            return;
        }
    } else {
        perror("Error: Activity log file does not exist.\n");
        turbo_mode = 0;
        input_disable = 0;
        return;
    }

    if ((fp = fopen(activity_log, "r")) == NULL) {
        perror("Error: Failed to open activity log file.\n");
        turbo_mode = 0;
        input_disable = 0;
        return;
    }

    if (fgets(buf, sizeof(buf), fp) == NULL) {
        if (feof(fp)) {
            printf("End of file reached.\n");
        } else if (ferror(fp)) {
            perror("Error: Failed to read from activity log file.\n");
        }
        fclose(fp);
        turbo_mode = 0;
        input_disable = 0;
        return;
    }

    int entry_index = 0;
    while (entry_index < 10 && fgets(buf, sizeof(buf), fp) != NULL) {
        char *name, *runtime;
        name = strtok(buf, ";");
        runtime = strtok(NULL, ";");

        if (name && runtime) {
            lv_obj_t* runtimeLabel = lblTrackerDetailRuntimes[entry_index];
            lv_obj_t* nameLabel = lblTrackerDetailNames[entry_index];
            lv_label_set_text(runtimeLabel, runtime);
            lv_label_set_text(nameLabel, name);
            entry_index++;
        }
    }

    fclose(fp);

    lv_obj_set_style_bg_color(ui_barTrackerProgress, lv_color_hex(0x403A03), LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_barTrackerProgress, 0, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    lv_obj_add_flag(ui_pnlTrackerProgress, LV_OBJ_FLAG_HIDDEN);
    lv_obj_clear_flag(ui_pnlTrackerContent, LV_OBJ_FLAG_HIDDEN);

    turbo_mode = 0;
    input_disable = 0;
}

void update_retroarch_playlist() {
    lv_refr_now(NULL);

    turbo_mode = 1;
    input_disable = 1;

    const char* compat = "/system/data/compat/compat.sh";
    char line[64];
    FILE* pipe = popen(compat, "r");

    if (!pipe) {
        perror("popen");
        turbo_mode = 0;
        input_disable = 0;
        return;
    }

    while (fgets(line, sizeof(line), pipe) != NULL) {
        int progress;
        char label[32];

        if (sscanf(line, "%d\t%99[^\n]", &progress, label) == 2) {
            lv_label_set_text(ui_lblUpdateProgress, label);
            lv_bar_set_value(ui_barUpdateProgress, progress, LV_ANIM_ON);
        }
    }

    pclose(pipe);

    turbo_mode = 0;
    input_disable = 0;
}

void reset_system_method() {
    char* opt;

    input_disable = 1;

    if (reset_method == -1) {
        opt = "FACTORYRESET";
    }
    
    if (reset_method == 1) {
        opt = "RETRORESET";
    }

    OptionSend option_array[] = {
        {opt, "true"}
    };

    size_t length = sizeof(option_array) / sizeof(option_array[0]);

    if (length > 0) {
        set_option_values(option_array, length);
        load_config(&options, &num_options);

        sleep(3);
        sync();
        reboot(RB_AUTOBOOT);
    }
}

void save_boot_options() {
    char theme[MAX_BUFFER_SIZE];
    char battery[MAX_BUFFER_SIZE];

    lv_dropdown_get_selected_str(ui_droTheme, theme, sizeof(theme));
    lv_dropdown_get_selected_str(ui_droBattery, battery, sizeof(battery));

    char *battery_level = strtok(battery, " ");
    if (battery_level != NULL) {
        strcpy(battery, battery_level);
    }

    OptionSend option_array[] = {
        {"SILENT", lv_obj_get_state(ui_chkSilent) & LV_STATE_CHECKED ? "true" : "false"},
        {"GAMEPAD", lv_obj_get_state(ui_chkGamepad) & LV_STATE_CHECKED ? "true" : "false"},
        {"ADB", lv_obj_get_state(ui_chkDebug) & LV_STATE_CHECKED ? "true" : "false"},
        {"HDMI", lv_obj_get_state(ui_chkHDMI) & LV_STATE_CHECKED ? "true" : "false"},
        {"THEME", theme},
        {"BATTERY", battery},
    };

    size_t length = sizeof(option_array) / sizeof(option_array[0]);
    set_option_values(option_array, length);

    load_config(&options, &num_options);
}

void restore_timer_options() {
    Option options_dro[] = {
        {"BLANK", get_option_value(options, num_options, "BLANK"), ui_droBlank},
        {"SHUTDOWN", get_option_value(options, num_options, "SHUTDOWN"), ui_droShutdown},
        {"LOWBATTERY", get_option_value(options, num_options, "LOWBATTERY"), ui_droLowBattery}
    };

    const char* blank_value = options_dro[0].value;
    const char* shutdown_value = options_dro[1].value;
    const char* lowbattery_value = options_dro[2].value;

    int blank_index = 0;
    int shutdown_index = 0;
    int lowbattery_index = 0;

    if (strcmp(blank_value, "300") == 0) {
        blank_index = 0;
    } else if (strcmp(blank_value, "600") == 0) {
        blank_index = 1;
    } else if (strcmp(blank_value, "900") == 0) {
        blank_index = 2;
    } else if (strcmp(blank_value, "1800") == 0) {
        blank_index = 3;
    } else if (strcmp(blank_value, "2700") == 0) {
        blank_index = 4;
    } else if (strcmp(blank_value, "3600") == 0) {
        blank_index = 5;
    } else if (strcmp(blank_value, "31536000") == 0) { // a year...
        blank_index = 6;
    }

    if (strcmp(shutdown_value, "300") == 0) {
        shutdown_index = 0;
    } else if (strcmp(shutdown_value, "600") == 0) {
        shutdown_index = 1;
    } else if (strcmp(shutdown_value, "900") == 0) {
        shutdown_index = 2;
    } else if (strcmp(shutdown_value, "1800") == 0) {
        shutdown_index = 3;
    } else if (strcmp(shutdown_value, "2700") == 0) {
        shutdown_index = 4;
    } else if (strcmp(shutdown_value, "3600") == 0) {
        shutdown_index = 5;
    } else if (strcmp(shutdown_value, "31536000") == 0) { // a year...
        shutdown_index = 6;
    }

    if (strcmp(lowbattery_value, "15") == 0) {
        lowbattery_index = 0;
    } else if (strcmp(lowbattery_value, "25") == 0) {
        lowbattery_index = 1;
    } else if (strcmp(lowbattery_value, "30") == 0) {
        lowbattery_index = 2;
    } else if (strcmp(lowbattery_value, "35") == 0) {
        lowbattery_index = 3;
    } else if (strcmp(lowbattery_value, "40") == 0) {
        lowbattery_index = 4;
    } else if (strcmp(lowbattery_value, "1") == 0) { // technically shouldn't reach this
        lowbattery_index = 5;
    }

    lv_dropdown_set_selected(ui_droBlank, blank_index);
    lv_dropdown_set_selected(ui_droShutdown, shutdown_index);
    lv_dropdown_set_selected(ui_droLowBattery, lowbattery_index);
}

void save_timer_options() {
    char blank[MAX_BUFFER_SIZE];
    char shutdown[MAX_BUFFER_SIZE];
    char lowbattery[MAX_BUFFER_SIZE];

    lv_dropdown_get_selected_str(ui_droBlank, blank, sizeof(blank));
    lv_dropdown_get_selected_str(ui_droShutdown, shutdown, sizeof(shutdown));
    lv_dropdown_get_selected_str(ui_droLowBattery, lowbattery, sizeof(lowbattery));

    if (strcmp(blank, "Disabled") == 0) {
        strcpy(blank, "31536000");
    } else {
        char *blank_level = strtok(blank, " ");

        int blank_value = atoi(blank_level);
        int blank_seconds = blank_value * 60;

        sprintf(blank, "%d", blank_seconds);
    }

    if (strcmp(shutdown, "Disabled") == 0) {
        strcpy(shutdown, "31536000");
    } else {
        char *shutdown_level = strtok(shutdown, " ");

        int shutdown_value = atoi(shutdown_level);
        int shutdown_seconds = shutdown_value * 60;

        sprintf(shutdown, "%d", shutdown_seconds);
    }

    if (strcmp(lowbattery, "Disabled") == 0) {
        strcpy(lowbattery, "0");
    } else {
        char *lowbattery_level = strtok(lowbattery, " ");
        strcpy(lowbattery, lowbattery_level);
    }

    OptionSend option_array[] = {
        {"BLANK", blank},
        {"SHUTDOWN", shutdown},
        {"LOWBATTERY", lowbattery}
    };

    size_t length = sizeof(option_array) / sizeof(option_array[0]);
    set_option_values(option_array, length);

    load_config(&options, &num_options);
}

void backup_options() {
    const char* con_reta = lv_obj_get_state(ui_chkConfigs) & LV_STATE_CHECKED ? "/mnt/mmc/MUOS/.retroarch/config" : "";
    const char* con_muos = lv_obj_get_state(ui_chkConfigs) & LV_STATE_CHECKED ? "/mnt/mmc/MUOS/muos.cfg" : "";
    const char* ra_plays = lv_obj_get_state(ui_chkPlaylists) & LV_STATE_CHECKED ? "/mnt/mmc/LIST" : "";
    const char* ra_saves = lv_obj_get_state(ui_chkSaves) & LV_STATE_CHECKED ? "/mnt/mmc/SAVE" : "";
    const char* ra_logs = lv_obj_get_state(ui_chkSaves) & LV_STATE_CHECKED ? "/mnt/mmc/LOGS" : "";
    const char* sys_opt = lv_obj_get_state(ui_chkSystem) & LV_STATE_CHECKED ? "/misc/options.txt" : "";
    const char* sys_bri = lv_obj_get_state(ui_chkSystem) & LV_STATE_CHECKED ? "/mnt/mmc/MUOS/.brightness" : "";
    const char* sys_vol = lv_obj_get_state(ui_chkSystem) & LV_STATE_CHECKED ? "/mnt/mmc/MUOS/.volume" : "";

    char archive_name[MAX_BUFFER_SIZE];
    time_t currentTime = time(NULL);

    if (currentTime == -1) {
        perror("Error getting current time");
    }

    snprintf(archive_name, sizeof(archive_name), "/mnt/mmc/muOS-backup-%ld.7z", currentTime);

    int compress_index = 0;
    const char* compress_files[8];

    if (strcmp(con_reta, "") != 0) {
        compress_files[compress_index] = strdup(con_reta);
        compress_index++;
    }
    if (strcmp(ra_plays, "") != 0) {
        compress_files[compress_index] = strdup(ra_plays);
        compress_index++;
    }
    if (strcmp(ra_saves, "") != 0) {
        compress_files[compress_index] = strdup(ra_saves);
        compress_index++;
    }
    if (strcmp(ra_logs, "") != 0) {
        compress_files[compress_index] = strdup(ra_logs);
        compress_index++;
    }
    if (strcmp(con_muos, "") != 0) {
        compress_files[compress_index] = strdup(con_muos);
        compress_index++;
    }
    if (strcmp(sys_opt, "") != 0) {
        compress_files[compress_index] = strdup(sys_opt);
        compress_index++;
    }
    if (strcmp(sys_bri, "") != 0) {
        compress_files[compress_index] = strdup(sys_bri);
        compress_index++;
    }
    if (strcmp(sys_vol, "") != 0) {
        compress_files[compress_index] = strdup(sys_vol);
        compress_index++;
    }

    input_disable = 1;

    /*
    The 7za command works just fine however if the amount to backup
    is quite small then the buffer just can't get a hold and nothing
    will be displayed on the progress bar, so instead we just send a
    message saying wether or not it worked.
    */
    if (create_archive(archive_name, compress_files, compress_index) != 0) {
        lv_bar_set_value(ui_barBackupProgress, 100, LV_ANIM_ON);
        lv_label_set_text(ui_lblBackupProgress, "Backup Failure");
    } else {
        lv_bar_set_value(ui_barBackupProgress, 100, LV_ANIM_ON);
        lv_label_set_text(ui_lblBackupProgress, "Backup Complete (TF1)");

        if (tf2_found == 1) {
            const char* sdcard_path = "/mnt/sdcard";
            char dest_path[MAX_BUFFER_SIZE];
            snprintf(dest_path, sizeof(dest_path), "%s/%s", sdcard_path, archive_name + strlen("/mnt/mmc/"));

            FILE* src_file = fopen(archive_name, "rb");
            if (src_file == NULL) {
                perror("Failed to source backup file");
            } else {
                FILE* dest_file = fopen(dest_path, "wb");
                if (dest_file == NULL) {
                    perror("Failed to open destination");
                } else {
                    char buffer[1024];
                    size_t bytes_read;
                    while ((bytes_read = fread(buffer, 1, sizeof(buffer), src_file)) > 0) {
                        size_t bytes_written = fwrite(buffer, 1, bytes_read, dest_file);
                        if (bytes_written != bytes_read) {
                            perror("Failed to write to destination file");
                            break;
                        }
                    }
                    fclose(dest_file);
                    lv_label_set_text(ui_lblBackupProgress, "Backup Complete (TF1+TF2)");
                }
                fclose(src_file);
            }
        }
    }

    cleanup_compressed_files(compress_files, compress_index);
    input_disable = 0;
}

void set_hardware_clock(struct rtc_time* rtc_tm) {
    int rtc_fd = open("/dev/rtc0", O_RDWR);
    if (rtc_fd == -1) {
        perror("Failed to open RTC device");
        return;
    }

    int result = ioctl(rtc_fd, RTC_SET_TIME, rtc_tm);
    if (result == -1) {
        perror("Failed to set hardware clock");
        close(rtc_fd);
        return;
    }

    close(rtc_fd);
}

void set_new_time() {
    struct tm newTime;
    struct timeval tv;
    struct rtc_time rtc_tm;

    char rolDay[MAX_BUFFER_SIZE];
    char rolMonth[MAX_BUFFER_SIZE];
    char rolYear[MAX_BUFFER_SIZE];
    char rolHour[MAX_BUFFER_SIZE];
    char rolMinute[MAX_BUFFER_SIZE];

    lv_roller_get_selected_str(ui_rolDay, rolDay, sizeof(rolDay));
    lv_roller_get_selected_str(ui_rolMonth, rolMonth, sizeof(rolMonth));
    lv_roller_get_selected_str(ui_rolYear, rolYear, sizeof(rolYear));
    lv_roller_get_selected_str(ui_rolHour, rolHour, sizeof(rolHour));
    lv_roller_get_selected_str(ui_rolMinute, rolMinute, sizeof(rolMinute));

    newTime.tm_mday = atoi(rolDay);
    newTime.tm_mon  = get_month_number(rolMonth);
    newTime.tm_year = atoi(rolYear) - 1900;
    newTime.tm_hour = atoi(rolHour);
    newTime.tm_min  = atoi(rolMinute);
    newTime.tm_sec  = 0;

    time_t newTimeSeconds = mktime(&newTime);

    tv.tv_sec = newTimeSeconds;
    tv.tv_usec = 0;

    rtc_tm.tm_sec = 0;
    rtc_tm.tm_min = newTime.tm_min;
    rtc_tm.tm_hour  = newTime.tm_hour;
    rtc_tm.tm_mday = newTime.tm_mday;
    rtc_tm.tm_mon = newTime.tm_mon;
    rtc_tm.tm_year = newTime.tm_year;
    rtc_tm.tm_wday = newTime.tm_wday;
    rtc_tm.tm_yday = newTime.tm_yday;
    rtc_tm.tm_isdst = 0;

    set_hardware_clock(&rtc_tm);
    settimeofday(&tv, NULL);
}

void run_dropdown_seq(lv_obj_t* widget) {
    if (lv_dropdown_is_open(widget)) {
        lv_dropdown_close(widget);
    } else {
        lv_dropdown_open(widget);
    }

    lv_event_send(widget, LV_EVENT_VALUE_CHANGED, NULL);
}

void run_checkbox_seq(lv_obj_t* widget) {
    if (lv_obj_has_state(widget, LV_STATE_CHECKED)) {
        lv_obj_clear_state(widget, LV_STATE_CHECKED);
    } else {
        lv_obj_add_state(widget, LV_STATE_CHECKED);
    }

    lv_event_send(widget, LV_EVENT_VALUE_CHANGED, NULL);
}

void run_screen_change(lv_obj_t* screen) {
    lv_scr_load(screen);
    active_screen = lv_scr_act();
}

void show_help_msgbox(lv_obj_t* panel, lv_obj_t* header_element, lv_obj_t* content_element, char* header_text, char* content_text) {
    if (msgbox_active == 0) {
        msgbox_active = 1;
        msgbox_element = panel;
        lv_label_set_text(header_element, header_text);
        lv_label_set_text(content_element, content_text);
        lv_obj_clear_flag(panel, LV_OBJ_FLAG_HIDDEN);
    }
}

void nav_prev(lv_group_t* group, int count) {
    int i;
    for (i = 0; i < count; i++) {
        lv_group_focus_prev(group);
    }
}

void nav_next(lv_group_t* group, int count) {
    int i;
    for (i = 0; i < count; i++) {
        lv_group_focus_next(group);
    }
}

lv_group_t* mainmenu_group;
lv_group_t* datetime_group;
lv_group_t* utility_group;
lv_group_t* tracker_group;
lv_group_t* update_group;
lv_group_t* extract_group;
lv_group_t* tester_group;
lv_group_t* cardtools_group;
lv_group_t* sysinfo_group;
lv_group_t* credits_group;
lv_group_t* bootoption_group;
lv_group_t* timer_group;
lv_group_t* reset_group;
lv_group_t* backup_group;
lv_group_t* setup_group;

void init_navigation_groups() {
    lv_obj_t* mainmenu_objects[] = {
        ui_btnDatetime,
        ui_btnUtilities,
        ui_btnBootOptions,
        ui_btnTimer,
        ui_btnReset,
        ui_btnBackup,
        ui_btnReboot,
        ui_btnRetroArch
    };

    lv_obj_t* datetime_objects[] = {
        ui_rolYear,
        ui_rolMonth,
        ui_rolDay,
        ui_rolHour,
        ui_rolMinute,
        ui_btnDatetimeCancel,
        ui_btnDatetimeSave
    };

    lv_obj_t* utility_objects[] = {
        ui_btnUtilitiesTracker,
        ui_btnUtilitiesExtract,
        ui_btnUtilitiesTester,
        ui_btnUtilitiesSDTools,
        ui_btnUtilitiesUpdate,
        ui_btnUtilitiesSysInfo,
        ui_btnUtilitiesCancel,
        ui_btnUtilitiesCredits
    };

    lv_obj_t* tracker_objects[] = {
        ui_btnTrackerCancel,
        ui_btnTrackerSummary
    };

    lv_obj_t* update_objects[] = {
        ui_btnUpdatePlaylist,
        ui_btnUpdateCancel
    };

    lv_obj_t* extract_objects[] = {
        ui_btnExtractCheats,
        ui_btnExtractCancel
    };

    lv_obj_t* tester_objects[] = {
        ui_btnTesterCancel
    };

    lv_obj_t* cardtools_objects[4] = {NULL};;

    if (detect_TF2_card()) {
        lv_label_set_text(ui_lblCardToolsMessage, "X: Format as FAT32\nY: Format as EXT4");
        lv_obj_set_style_img_recolor(ui_imgTF2Card, lv_color_hex(0xDDDDDD), LV_PART_MAIN | LV_STATE_DEFAULT);
        lv_obj_set_style_text_color(ui_lblTF2Card, lv_color_hex(0xDDDDDD), LV_PART_MAIN | LV_STATE_DEFAULT);
        cardtools_objects[0] = ui_btnCardToolsFormat;
        cardtools_objects[1] = ui_btnCardToolsSync;
        cardtools_objects[2] = ui_btnCardToolsHealth;
        cardtools_objects[3] = ui_btnCardToolsCancel;
    } else {
        lv_label_set_text(ui_lblCardToolsMessage, "");
        lv_obj_add_state(ui_btnCardToolsFormat, LV_STATE_DISABLED);
        lv_obj_add_state(ui_btnCardToolsSync, LV_STATE_DISABLED);
        cardtools_objects[0] = ui_btnCardToolsHealth;
        cardtools_objects[1] = ui_btnCardToolsCancel;
    }

    lv_obj_t* sysinfo_objects[] = {
        ui_btnSysInfoBack
    };

    lv_obj_t* credits_objects[] = {
        ui_btnCreditsBack
    };

    lv_obj_t* bootoption_objects[] = {
        ui_chkSilent,
        ui_chkGamepad,
        ui_chkDebug,
        ui_chkHDMI,
        ui_droTheme,
        ui_droBattery,
        ui_btnBootCancel,
        ui_btnBootSave
    };

    lv_obj_t* timer_objects[] = {
        ui_droBlank,
        ui_droShutdown,
        ui_droLowBattery,
        ui_btnTimerCancel,
        ui_btnTimerSave
    };

    lv_obj_t* reset_objects[] = {
        ui_droReset,
        ui_btnResetCancel,
        ui_btnResetConfirm
    };

    lv_obj_t* backup_objects[] = {
        ui_chkConfigs,
        ui_chkPlaylists,
        ui_chkSaves,
        ui_chkSystem,
        ui_btnBackupCancel,
        ui_btnBackupConfirm
    };

    lv_obj_t* setup_objects[] = {
        ui_chkWinCompat,
        ui_chkSilentSetup,
        ui_chkDebugSetup,
        ui_droSetupBattery,
        ui_droSetupCard,
        ui_btnSetupContinue
    };

    mainmenu_group = lv_group_create();
    datetime_group = lv_group_create();
    utility_group = lv_group_create();
    tracker_group = lv_group_create();
    update_group = lv_group_create();
    extract_group = lv_group_create();
    tester_group = lv_group_create();
    cardtools_group = lv_group_create();
    sysinfo_group = lv_group_create();
    credits_group = lv_group_create();
    bootoption_group = lv_group_create();
    timer_group = lv_group_create();
    reset_group = lv_group_create();
    backup_group = lv_group_create();
    setup_group = lv_group_create();

    for (int i = 0; i < sizeof(mainmenu_objects) / sizeof(mainmenu_objects[0]); i++) {
        lv_group_add_obj(mainmenu_group, mainmenu_objects[i]);
    }

    for (int i = 0; i < sizeof(datetime_objects) / sizeof(datetime_objects[0]); i++) {
        lv_group_add_obj(datetime_group, datetime_objects[i]);
    }

    for (int i = 0; i < sizeof(utility_objects) / sizeof(utility_objects[0]); i++) {
        lv_group_add_obj(utility_group, utility_objects[i]);
    }

    for (int i = 0; i < sizeof(tracker_objects) / sizeof(tracker_objects[0]); i++) {
        lv_group_add_obj(tracker_group, tracker_objects[i]);
    }

    for (int i = 0; i < sizeof(update_objects) / sizeof(update_objects[0]); i++) {
        lv_group_add_obj(update_group, update_objects[i]);
    }

    for (int i = 0; i < sizeof(extract_objects) / sizeof(extract_objects[0]); i++) {
        lv_group_add_obj(extract_group, extract_objects[i]);
    }

    for (int i = 0; i < sizeof(tester_objects) / sizeof(tester_objects[0]); i++) {
        lv_group_add_obj(tester_group, tester_objects[i]);
    }

    for (int i = 0; i < sizeof(cardtools_objects) / sizeof(cardtools_objects[0]); i++) {
        if (cardtools_objects[i] != NULL) {
            lv_group_add_obj(cardtools_group, cardtools_objects[i]);
        }
    }

    for (int i = 0; i < sizeof(sysinfo_objects) / sizeof(sysinfo_objects[0]); i++) {
        lv_group_add_obj(sysinfo_group, sysinfo_objects[i]);
    }

    for (int i = 0; i < sizeof(credits_objects) / sizeof(credits_objects[0]); i++) {
        lv_group_add_obj(credits_group, credits_objects[i]);
    }

    for (int i = 0; i < sizeof(bootoption_objects) / sizeof(bootoption_objects[0]); i++) {
        lv_group_add_obj(bootoption_group, bootoption_objects[i]);
    }

    for (int i = 0; i < sizeof(timer_objects) / sizeof(timer_objects[0]); i++) {
        lv_group_add_obj(timer_group, timer_objects[i]);
    }

    for (int i = 0; i < sizeof(reset_objects) / sizeof(reset_objects[0]); i++) {
        lv_group_add_obj(reset_group, reset_objects[i]);
    }

    for (int i = 0; i < sizeof(backup_objects) / sizeof(backup_objects[0]); i++) {
        lv_group_add_obj(backup_group, backup_objects[i]);
    }

    for (int i = 0; i < sizeof(setup_objects) / sizeof(setup_objects[0]); i++) {
        lv_group_add_obj(setup_group, setup_objects[i]);
    }

    lv_gridnav_add(ui_scrMainmenu, LV_GRIDNAV_CTRL_NONE);
}

static void joystick_task() {
    struct js_event ev;

    while (1) {
        if (input_disable == 1) {
            continue;
        }
        int input_text = strlen(lv_textarea_get_text(ui_txtTesterLog));
        if (input_text > 180) {
            lv_textarea_set_text(ui_txtTesterLog, "");
        }
        read(js_fd, &ev, sizeof(struct js_event));
        switch (ev.type) {
            case JS_EVENT_BUTTON:
                if (ev.value == 1 && ev.number == JOY_SELECT && msgbox_active == 1) {
                    msgbox_active = 0;
                    lv_obj_add_flag(msgbox_element, LV_OBJ_FLAG_HIDDEN);
                    break;
                } else {
                    if (msgbox_active == 0 && ev.value == 0) {
                        if (active_screen == ui_scrTester) {
                            if (ev.number == JOY_MENU) {
                                lv_obj_add_flag(ui_imgTesterInputMenu, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_MENU RELEASE\n");
                            }
                            if (ev.number == JOY_SELECT) {
                                lv_obj_add_flag(ui_imgTesterInputSelect, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_SELECT RELEASE\n");
                            }
                            if (ev.number == JOY_START) {
                                lv_obj_add_flag(ui_imgTesterInputStart, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_START RELEASE\n");
                            }
                            if (ev.number == JOY_A) {
                                lv_obj_add_flag(ui_imgTesterInputButtonA, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_A RELEASE\n");
                            }
                            if (ev.number == JOY_B) {
                                lv_obj_add_flag(ui_imgTesterInputButtonB, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_B RELEASE\n");
                            }
                            if (ev.number == JOY_X) {
                                lv_obj_add_flag(ui_imgTesterInputButtonX, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_X RELEASE\n");
                            }
                            if (ev.number == JOY_Y) {
                                lv_obj_add_flag(ui_imgTesterInputButtonY, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_Y RELEASE\n");
                            }
                            if (ev.number == JOY_R1) {
                                lv_obj_add_flag(ui_imgTesterInputR1, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_R1 RELEASE\n");
                            }
                            if (ev.number == JOY_L1) {
                                lv_obj_add_flag(ui_imgTesterInputL1, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_L1 RELEASE\n");
                            }
                        }
                    }
                    else if (msgbox_active == 0 && ev.value == 1) {
                        if (active_screen == ui_scrReset && msgbox_active == 0) {
                            if (ev.number == JOY_START) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(reset_group);
                                if (element_focused == ui_btnResetConfirm) {
                                    reset_system_method();
                                }
                            }
                        }
                        if (active_screen == ui_scrCardTools && msgbox_active == 0) {
                            if (ev.number == JOY_L1) {
                                lv_textarea_cursor_up(ui_txtCardToolsInfo);
                            }
                            else if (ev.number == JOY_R1) {
                                lv_textarea_cursor_down(ui_txtCardToolsInfo);
                            }
                        }
                        if (active_screen == ui_scrTester && msgbox_active == 0) {
                            if (ev.number == JOY_POWER) {
                                run_screen_change(ui_scrUtilities);
                                lv_textarea_set_text(ui_txtTesterLog, "");
                            }
                            if (ev.number == JOY_MENU) {
                                lv_obj_clear_flag(ui_imgTesterInputMenu, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_MENU PRESS\n");
                            }
                            if (ev.number == JOY_SELECT) {
                                lv_obj_clear_flag(ui_imgTesterInputSelect, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_SELECT PRESS\n");
                            }
                            if (ev.number == JOY_START) {
                                lv_obj_clear_flag(ui_imgTesterInputStart, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_START PRESS\n");
                            }
                            if (ev.number == JOY_A) {
                                lv_obj_clear_flag(ui_imgTesterInputButtonA, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_A PRESS\n");
                            }
                            if (ev.number == JOY_B) {
                                lv_obj_clear_flag(ui_imgTesterInputButtonB, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_B PRESS\n");
                            }
                            if (ev.number == JOY_X) {
                                lv_obj_clear_flag(ui_imgTesterInputButtonX, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_X PRESS\n");
                            }
                            if (ev.number == JOY_Y) {
                                lv_obj_clear_flag(ui_imgTesterInputButtonY, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_Y PRESS\n");
                            }
                            if (ev.number == JOY_R1) {
                                lv_obj_clear_flag(ui_imgTesterInputR1, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_R1 PRESS\n");
                            }
                            if (ev.number == JOY_L1) {
                                lv_obj_clear_flag(ui_imgTesterInputL1, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_R2 PRESS\n");
                            }
                        }
                        else if (ev.number == JOY_SELECT && msgbox_active == 0) {
                            if (active_screen == ui_scrMainmenu) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(mainmenu_group);
                                if (element_focused == ui_btnDatetime) {
                                    show_help_msgbox(ui_pnlMainmenuHelp,
                                            ui_lblMainmenuHelpMessageHeader, ui_lblMainmenuHelpMessageContent,
                                               "Date and Time", "Change the local system date and time.");
                                }
                                else if (element_focused == ui_btnUtilities) {
                                    show_help_msgbox(ui_pnlMainmenuHelp,
                                            ui_lblMainmenuHelpMessageHeader, ui_lblMainmenuHelpMessageContent,
                                               "Utilities", "Ah! The utility room! Where I'm the janitor and you're... the janitors wife!");
                                }
                                else if (element_focused == ui_btnBootOptions) {
                                    show_help_msgbox(ui_pnlMainmenuHelp,
                                            ui_lblMainmenuHelpMessageHeader, ui_lblMainmenuHelpMessageContent,
                                               "Boot Options", "Change the functionality of the device including the boot theme, battery type, and enabling ADB mode.");
                                }
                                else if (element_focused == ui_btnTimer) {
                                    show_help_msgbox(ui_pnlMainmenuHelp, 
                                            ui_lblMainmenuHelpMessageHeader, ui_lblMainmenuHelpMessageContent,
                                               "System Timers", "Adjust the timing for screen blanking, shutdown, and low battery alerts.");
                                }
                                else if (element_focused == ui_btnReset) {
                                    show_help_msgbox(ui_pnlMainmenuHelp,
                                            ui_lblMainmenuHelpMessageHeader, ui_lblMainmenuHelpMessageContent,
                                               "Reset", "Reset the RetroArch configuration or factory reset the device.");
                                }
                                else if (element_focused == ui_btnBackup) {
                                    show_help_msgbox(ui_pnlMainmenuHelp, 
                                            ui_lblMainmenuHelpMessageHeader, ui_lblMainmenuHelpMessageContent,
                                               "Backup", "Will backup all of your settings, save states, and core configs, into a single 7Zip file on either TF1 or TF2 (if present).");
                                }
                                else if (element_focused == ui_btnReboot) {
                                    show_help_msgbox(ui_pnlMainmenuHelp, 
                                            ui_lblMainmenuHelpMessageHeader, ui_lblMainmenuHelpMessageContent,
                                               "Reboot", "Reboots the device, no questions asked.");
                                }
                                else if (element_focused == ui_btnRetroArch) {
                                    show_help_msgbox(ui_pnlMainmenuHelp,
                                            ui_lblMainmenuHelpMessageHeader, ui_lblMainmenuHelpMessageContent,
                                               "Back to RetroArch", "Go back to the RetroArch system.");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrDatetime) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(datetime_group);
                                if (element_focused == ui_rolYear) {
                                    show_help_msgbox(ui_pnlDatetimeHelp,
                                            ui_lblDatetimeHelpMessageHeader, ui_lblDatetimeHelpMessageContent,
                                               "Year", "The current year. Unfortunately time travel does not exist.\n\nyet...");
                                }
                                else if (element_focused == ui_rolMonth) {
                                    show_help_msgbox(ui_pnlDatetimeHelp,
                                            ui_lblDatetimeHelpMessageHeader, ui_lblDatetimeHelpMessageContent,
                                               "Month", "Representation of the specific month. Lousy smarch weather.");
                                }
                                else if (element_focused == ui_rolDay) {
                                    show_help_msgbox(ui_pnlDatetimeHelp,
                                            ui_lblDatetimeHelpMessageHeader, ui_lblDatetimeHelpMessageContent,
                                               "Day", "Indicates the specific day within a month. Will roll back if year or month changes.");
                                }
                                else if (element_focused == ui_rolHour) {
                                    show_help_msgbox(ui_pnlDatetimeHelp,
                                            ui_lblDatetimeHelpMessageHeader, ui_lblDatetimeHelpMessageContent,
                                               "Hour", "Denotes the specific hour component in a 24-hour format.");
                                }
                                else if (element_focused == ui_rolMinute) {
                                    show_help_msgbox(ui_pnlDatetimeHelp,
                                            ui_lblDatetimeHelpMessageHeader, ui_lblDatetimeHelpMessageContent,
                                               "Minute", "Represents the specific minute component within an hour.");
                                }
                                else if (element_focused == ui_btnDatetimeCancel) {
                                    show_help_msgbox(ui_pnlDatetimeHelp,
                                            ui_lblDatetimeHelpMessageHeader, ui_lblDatetimeHelpMessageContent,
                                               "Cancel", "Ignore any changes made to the date and time.");
                                }
                                else if (element_focused == ui_btnDatetimeSave) {
                                    show_help_msgbox(ui_pnlDatetimeHelp,
                                            ui_lblDatetimeHelpMessageHeader, ui_lblDatetimeHelpMessageContent,
                                               "Save", "Save the changes to the date and time. This will change the current system date and time.");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrUtilities) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(utility_group);
                                if (element_focused == ui_btnUtilitiesTracker) {
                                    show_help_msgbox(ui_pnlUtilitiesHelp,
                                            ui_lblUtilitiesHelpMessageHeader, ui_lblUtilitiesHelpMessageContent,
                                               "Activity Tracker", "See how much time you've spent on different systems and games (currently unavailable).");
                                }
                                else if (element_focused == ui_btnUtilitiesExtract) {
                                    show_help_msgbox(ui_pnlUtilitiesHelp,
                                            ui_lblUtilitiesHelpMessageHeader, ui_lblUtilitiesHelpMessageContent,
                                               "Extraction", "Handy content to extract into RetroArch for convenience.");
                                }
                                else if (element_focused == ui_btnUtilitiesTester) {
                                    show_help_msgbox(ui_pnlUtilitiesHelp,
                                            ui_lblUtilitiesHelpMessageHeader, ui_lblUtilitiesHelpMessageContent,
                                               "Input Tester", "A simple input tester for the RG35XX buttons and triggers.");
                                }
                                else if (element_focused == ui_btnUtilitiesSDTools) {
                                    show_help_msgbox(ui_pnlUtilitiesHelp,
                                            ui_lblUtilitiesHelpMessageHeader, ui_lblUtilitiesHelpMessageContent,
                                               "SD Card Tools", "Format the second SD card, sync BIOS files across cards, or check the health of SD cards.");
                                }
                                else if (element_focused == ui_btnUtilitiesUpdate) {
                                    show_help_msgbox(ui_pnlUtilitiesHelp,
                                            ui_lblUtilitiesHelpMessageHeader, ui_lblUtilitiesHelpMessageContent,
                                               "Playlist Update", "Update the RetroArch playlists based on the contents of the ROMS directory.");
                                }
                                else if (element_focused == ui_btnUtilitiesSysInfo) {
                                    show_help_msgbox(ui_pnlUtilitiesHelp,
                                            ui_lblUtilitiesHelpMessageHeader, ui_lblUtilitiesHelpMessageContent,
                                               "System Info", "See a quick overview of system information.");
                                }
                                else if (element_focused == ui_btnUtilitiesCredits) {
                                    show_help_msgbox(ui_pnlUtilitiesHelp,
                                            ui_lblUtilitiesHelpMessageHeader, ui_lblUtilitiesHelpMessageContent,
                                               "Credits", "A nice screen to thank all of the people who contributed to muOS in any way.");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrTracker) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(tracker_group);
                                if (element_focused == ui_btnTrackerSummary) {
                                    show_help_msgbox(ui_pnlTrackerHelp,
                                            ui_lblTrackerHelpMessageHeader, ui_lblTrackerHelpMessageContent,
                                               "Summary", "View a summary of everything you've done on the device.");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrUpdate) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(update_group);
                                if (element_focused == ui_btnUpdatePlaylist) {
                                    show_help_msgbox(ui_pnlUpdateHelp,
                                            ui_lblUpdateHelpMessageHeader, ui_lblUpdateHelpMessageContent,
                                               "Update", "Updates the RetroArch playlists based on the content in ROMS.");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrExtract) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(bootoption_group);
                                if (element_focused == ui_btnExtractCheats) {
                                    show_help_msgbox(ui_pnlBootOptionHelp,
                                            ui_lblBootOptionHelpMessageHeader, ui_lblBootOptionHelpMessageContent,
                                               "RetroArch Cheats", "Cheats for various systems to use with RetroArch. This may take a while to extract so be patient!");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrCredits) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(credits_group);
                                if (element_focused == ui_btnCreditsBack) {
                                    lv_obj_clear_flag(ui_imgCreditsSecret, LV_OBJ_FLAG_HIDDEN);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrBootOption) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(bootoption_group);
                                if (element_focused == ui_chkSilent) {
                                    show_help_msgbox(ui_pnlBootOptionHelp,
                                            ui_lblBootOptionHelpMessageHeader, ui_lblBootOptionHelpMessageContent,
                                               "Silent Boot", "I've lost the bleeps, I've lost the sweeps, and I've lost the creeps! Disables any sound or rumble that a theme may use.");
                                }
                                else if (element_focused == ui_chkGamepad) {
                                    show_help_msgbox(ui_pnlBootOptionHelp,
                                            ui_lblBootOptionHelpMessageHeader, ui_lblBootOptionHelpMessageContent,
                                               "Gamepad Wait", "Waits a few more seconds on boot so that external gamepads initialise properly.");
                                }
                                else if (element_focused == ui_chkDebug) {
                                    show_help_msgbox(ui_pnlBootOptionHelp,
                                            ui_lblBootOptionHelpMessageHeader, ui_lblBootOptionHelpMessageContent,
                                               "Enable ADB", "Android Debug Bridge. Useful for developers or those who are sick of removing the SD card all the time!");
                                }
                                else if (element_focused == ui_chkHDMI) {
                                    show_help_msgbox(ui_pnlBootOptionHelp,
                                            ui_lblBootOptionHelpMessageHeader, ui_lblBootOptionHelpMessageContent,
                                               "Enable HDMI", "(Currently not working) Play the device on a much bigger screen. Uses a bit more resources but not too much.");
                                }
                                else if (element_focused == ui_droTheme) {
                                    show_help_msgbox(ui_pnlBootOptionHelp,
                                            ui_lblBootOptionHelpMessageHeader, ui_lblBootOptionHelpMessageContent,
                                               "Boot Theme", "What do you want displayed on the device as it first boots up? You can make your own too!");
                                }
                                else if (element_focused == ui_droBattery) {
                                    show_help_msgbox(ui_pnlBootOptionHelp,
                                            ui_lblBootOptionHelpMessageHeader, ui_lblBootOptionHelpMessageContent,
                                               "Battery Type", "What type of battery do you have in this device? Most devices come with a 2600 battery as default.");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrTimer) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(timer_group);
                                if (element_focused == ui_droBlank) {
                                    show_help_msgbox(ui_pnlTimerHelp,
                                            ui_lblTimerHelpMessageHeader, ui_lblTimerHelpMessageContent,
                                               "Idle Blank", "How long should the device wait before switching the screen off?");
                                }
                                else if (element_focused == ui_droShutdown) {
                                    show_help_msgbox(ui_pnlTimerHelp,
                                            ui_lblTimerHelpMessageHeader, ui_lblTimerHelpMessageContent,
                                               "Idle Shutdown", "How long should the device wait before completely shutting down?");
                                }
                                else if (element_focused == ui_droLowBattery) {
                                    show_help_msgbox(ui_pnlTimerHelp,
                                            ui_lblTimerHelpMessageHeader, ui_lblTimerHelpMessageContent,
                                               "Low Battery", "At what percentage should the screen flash if the battery is getting low?");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrReset) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(reset_group);
                                if (element_focused == ui_droReset) {
                                    show_help_msgbox(ui_pnlResetHelp,
                                            ui_lblResetHelpMessageHeader, ui_lblResetHelpMessageContent,
                                               "Type of Reset", "Something went wrong? That's okay mistakes can happen. Restore the default RetroArch configurations first before trying wiping the entire system.");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrBackup) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(backup_group);
                                if (element_focused == ui_chkConfigs) {
                                    show_help_msgbox(ui_pnlBackupHelp,
                                            ui_lblBackupHelpMessageHeader, ui_lblBackupHelpMessageContent,
                                               "RetroArch Configurations", "Made some custom changes to games or cores, you might want to save those!");
                                }
                                else if (element_focused == ui_chkPlaylists) {
                                    show_help_msgbox(ui_pnlBackupHelp,
                                            ui_lblBackupHelpMessageHeader, ui_lblBackupHelpMessageContent,
                                               "RetroArch Playlists", "Precious playlists can take quite a while to make. Back these up for sure!");
                                }
                                else if (element_focused == ui_chkSaves) {
                                    show_help_msgbox(ui_pnlBackupHelp,
                                            ui_lblBackupHelpMessageHeader, ui_lblBackupHelpMessageContent,
                                               "Save Files and Save States", "You finally got that digital monster you've always wanted! Don't let it get away, back up your save games!");
                                }
                                else if (element_focused == ui_chkSystem) {
                                    show_help_msgbox(ui_pnlBackupHelp,
                                            ui_lblBackupHelpMessageHeader, ui_lblBackupHelpMessageContent,
                                               "System Settings", "Backup the brightness, volume, and the boot options.");
                                }
                                break;
                            }
                            else if (active_screen == ui_scrSetup) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(setup_group);
                                if (element_focused == ui_chkWinCompat) {
                                    show_help_msgbox(ui_pnlSetupHelp,
                                            ui_lblSetupHelpMessageHeader, ui_lblSetupHelpMessageContent,
                                               "Windows Compatibility", "");
                                }
                                else if (element_focused == ui_chkSilentSetup) {
                                    show_help_msgbox(ui_pnlSetupHelp,
                                            ui_lblSetupHelpMessageHeader, ui_lblSetupHelpMessageContent,
                                               "Complete Silent Setup", "");
                                }
                                else if (element_focused == ui_chkDebugSetup) {
                                    show_help_msgbox(ui_pnlSetupHelp,
                                            ui_lblSetupHelpMessageHeader, ui_lblSetupHelpMessageContent,
                                               "Enable ADB", "");
                                }
                                else if (element_focused == ui_droSetupBattery) {
                                    show_help_msgbox(ui_pnlSetupHelp,
                                            ui_lblSetupHelpMessageHeader, ui_lblSetupHelpMessageContent,
                                               "Battery Type", "");
                                }
                                else if (element_focused == ui_droSetupCard) {
                                    show_help_msgbox(ui_pnlSetupHelp,
                                            ui_lblSetupHelpMessageHeader, ui_lblSetupHelpMessageContent,
                                               "SD Card Setup", "");
                                }
                                break;
                            }
                        }
                        else if (ev.number == JOY_X && msgbox_active == 0) {
                            if (active_screen == ui_scrCardTools) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(cardtools_group);
                                if (element_focused == ui_btnCardToolsFormat) {
                                    format_sdcard("vfat", "0b");
                                }
                                break;
                            }
                        }
                        else if (ev.number == JOY_Y && msgbox_active == 0) {
                            if (active_screen == ui_scrCardTools) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(cardtools_group);
                                if (element_focused == ui_btnCardToolsFormat) {
                                    format_sdcard("ext4", "83");
                                }
                                break;
                            }
                        }
                        else if (ev.number == JOY_A && msgbox_active == 0) {
                            if (active_screen == ui_scrMainmenu) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(mainmenu_group);
                                if (element_focused == ui_btnDatetime) {
                                    run_screen_change(ui_scrDatetime);
                                    restore_clock_settings();
                                }
                                else if (element_focused == ui_btnUtilities) {
                                    run_screen_change(ui_scrUtilities);
                                }
                                else if (element_focused == ui_btnBootOptions) {
                                    run_screen_change(ui_scrBootOption);
                                    restore_boot_options();
                                }
                                else if (element_focused == ui_btnTimer) {
                                    run_screen_change(ui_scrTimer);
                                    restore_timer_options();
                                }
                                else if (element_focused == ui_btnReset) {
                                    run_screen_change(ui_scrReset);
                                }
                                else if (element_focused == ui_btnBackup) {
                                    run_screen_change(ui_scrBackup);
                                }
                                else if (element_focused == ui_btnReboot) {
                                    sync();
                                    reboot(RB_AUTOBOOT);
                                }
                                else if (element_focused == ui_btnRetroArch) {
                                    exit(EXIT_SUCCESS);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrDatetime) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(datetime_group);
                                if (element_focused == ui_btnDatetimeCancel) {
                                    run_screen_change(ui_scrMainmenu);
                                }
                                else if (element_focused == ui_btnDatetimeSave) {
                                    set_new_time();
                                    run_screen_change(ui_scrMainmenu);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrUtilities) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(utility_group);
                                if (element_focused == ui_btnUtilitiesTracker) {
                                    run_screen_change(ui_scrTracker);
                                    update_activity_tracker("/mnt/mmc/LOGS/activity.csv");
                                }
                                else if (element_focused == ui_btnUtilitiesExtract) {
                                    run_screen_change(ui_scrExtract);
                                }
                                else if (element_focused == ui_btnUtilitiesTester) {
                                    run_screen_change(ui_scrTester);
                                }
                                else if (element_focused == ui_btnUtilitiesSDTools) {
                                    run_screen_change(ui_scrCardTools);
                                }
                                else if (element_focused == ui_btnUtilitiesUpdate) {
                                    run_screen_change(ui_scrUpdate);
                                }
                                else if (element_focused == ui_btnUtilitiesSysInfo) {
                                    update_system_info();
                                    run_screen_change(ui_scrSysInfo);
                                }
                                else if (element_focused == ui_btnUtilitiesCancel) {
                                    run_screen_change(ui_scrMainmenu);
                                }
                                else if (element_focused == ui_btnUtilitiesCredits) {
                                    run_screen_change(ui_scrCredits);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrTracker) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(tracker_group);
                                if (element_focused == ui_btnTrackerCancel) {
                                    run_screen_change(ui_scrUtilities);
                                }
                                else if (element_focused == ui_btnTrackerSummary) {
                                    // todo
                                }
                                break;
                            }
                            else if (active_screen == ui_scrUpdate) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(update_group);
                                if (element_focused == ui_btnUpdateCancel) {
                                    run_screen_change(ui_scrUtilities);
                                }
                                else if (element_focused == ui_btnUpdatePlaylist) {
                                    update_retroarch_playlist();
                                }
                                break;
                            }
                            else if (active_screen == ui_scrExtract) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(extract_group);
                                if (element_focused == ui_btnExtractCheats) {
                                    //extract_archive("/mnt/mmc/MUOS/extra/cheat.7z", "/mnt/mmc");
                                }
                                else if (element_focused == ui_btnExtractCancel) {
                                    run_screen_change(ui_scrUtilities);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrCardTools) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(cardtools_group);
                                if (element_focused == ui_btnCardToolsSync) {
                                    input_disable = 1;
                                    const char* mmc_bios = "/mnt/mmc/BIOS";
                                    const char* sd_bios = "/mnt/sdcard/BIOS";
                                    lv_textarea_set_text(ui_txtCardToolsInfo, "");
                                    lv_textarea_add_text(ui_txtCardToolsInfo, "Syncing TF1 (mmc) BIOS to TF2 (sdcard) BIOS\n");
                                    execute_sync(mmc_bios, sd_bios, ui_txtCardToolsInfo);
                                    lv_textarea_add_text(ui_txtCardToolsInfo, "TF1 to TF2 sync complete\n\n");
                                    usleep(100);
                                    lv_textarea_add_text(ui_txtCardToolsInfo, "Syncing TF2 (sdcard) BIOS to TF1 (mmc) BIOS\n");
                                    execute_sync(sd_bios, mmc_bios, ui_txtCardToolsInfo);
                                    lv_textarea_add_text(ui_txtCardToolsInfo, "TF2 to TF1 sync complete\n");
                                    input_disable = 0;
                                }
                                else if (element_focused == ui_btnCardToolsHealth) {
                                    input_disable = 1;
                                    lv_textarea_set_text(ui_txtCardToolsInfo, "");
                                    lv_textarea_add_text(ui_txtCardToolsInfo, "Checking TF1 BOOT\n");
                                    run_fsck_health_check("0p1");
                                    usleep(100);
                                    lv_textarea_add_text(ui_txtCardToolsInfo, "\nChecking TF1 ROMS\n");
                                    run_fsck_health_check("0p3");
                                    usleep(100);
                                    if (tf2_found == 1) {
                                        lv_textarea_add_text(ui_txtCardToolsInfo, "\nChecking TF2 ROMS\n");
                                        run_fsck_health_check("1p1");
                                        usleep(100);
                                    }
                                    input_disable = 0;
                                }
                                else if (element_focused == ui_btnCardToolsCancel) {
                                    run_screen_change(ui_scrUtilities);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrSysInfo) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(sysinfo_group);
                                if (element_focused == ui_btnSysInfoBack) {
                                    run_screen_change(ui_scrUtilities);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrCredits) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(credits_group);
                                if (element_focused == ui_btnCreditsBack) {
                                    lv_obj_add_flag(ui_imgCreditsSecret, LV_OBJ_FLAG_HIDDEN);
                                    run_screen_change(ui_scrUtilities);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrBootOption) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(bootoption_group);
                                if (element_focused == ui_chkSilent ||
                                    element_focused == ui_chkHDMI ||
                                    element_focused == ui_chkGamepad ||
                                    element_focused == ui_chkDebug) {
                                    run_checkbox_seq(element_focused);
                                }
                                else if (element_focused == ui_droTheme ||
                                    element_focused == ui_droBattery) {
                                    run_dropdown_seq(element_focused);
                                }
                                else if (element_focused == ui_btnBootCancel) {
                                    run_screen_change(ui_scrMainmenu);
                                }
                                else if (element_focused == ui_btnBootSave) {
                                    save_boot_options();
                                    run_screen_change(ui_scrMainmenu);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrTimer) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(timer_group);
                                if (element_focused == ui_droLowBattery ||
                                    element_focused == ui_droBlank ||
                                    element_focused == ui_droShutdown) {
                                    run_dropdown_seq(element_focused);
                                }
                                else if (element_focused == ui_btnTimerCancel) {
                                    run_screen_change(ui_scrMainmenu);
                                }
                                else if (element_focused == ui_btnTimerSave) {
                                    save_timer_options();
                                    run_screen_change(ui_scrMainmenu);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrReset) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(reset_group);
                                if (element_focused == ui_droReset) {
                                    run_dropdown_seq(element_focused);
                                }
                                else if (element_focused == ui_btnResetCancel) {
                                    run_screen_change(ui_scrMainmenu);
                                }
                                break;
                            }
                            else if (active_screen == ui_scrBackup) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(backup_group);
                                if (element_focused == ui_chkConfigs ||
                                    element_focused == ui_chkPlaylists ||
                                    element_focused == ui_chkSaves ||
                                    element_focused == ui_chkSystem) {
                                    run_checkbox_seq(element_focused);
                                }
                                else if (element_focused == ui_btnBackupCancel) {
                                    lv_bar_set_value(ui_barBackupProgress, 0, LV_ANIM_ON);
                                    lv_label_set_text(ui_lblBackupProgress, "");
                                    lv_obj_clear_state(ui_chkConfigs, LV_STATE_CHECKED);
                                    lv_obj_clear_state(ui_chkPlaylists, LV_STATE_CHECKED);
                                    lv_obj_clear_state(ui_chkSaves, LV_STATE_CHECKED);
                                    lv_obj_clear_state(ui_chkSystem, LV_STATE_CHECKED);
                                    run_screen_change(ui_scrMainmenu);
                                }
                                else if (element_focused == ui_btnBackupConfirm) {
                                    backup_options();
                                }
                                break;
                            }
                            else if (active_screen == ui_scrSetup) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(setup_group);
                                if (element_focused == ui_chkWinCompat) {
                                    run_checkbox_seq(element_focused);
                                }
                                else if (element_focused == ui_chkSilentSetup) {
                                    run_checkbox_seq(element_focused);
                                }
                                else if (element_focused == ui_chkDebugSetup) {
                                    run_checkbox_seq(element_focused);
                                }
                                else if (element_focused == ui_droSetupBattery) {
                                    run_dropdown_seq(element_focused);
                                }
                                else if (element_focused == ui_droSetupCard) {
                                    run_dropdown_seq(element_focused);
                                }
                                else if (element_focused == ui_btnSetupContinue) {
                                    printf("todo...\n");
                                }
                                break;
                            }
                        }
                    }
                }
                break;
            case JS_EVENT_AXIS:
                if (msgbox_active == 1) {
                    break;
                } else {
                    if (active_screen == ui_scrTester && msgbox_active == 0) {
                        if (ev.value == 0) {
                            if (ev.number == JOY_UP && input_test_down == 0) {
                                lv_obj_add_flag(ui_imgTesterInputDpadUp, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_UP RELEASE\n");
                            }
                            else if (ev.number == JOY_DOWN && input_test_down == 1) {
                                input_test_down = 0;
                                lv_obj_add_flag(ui_imgTesterInputDpadDown, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_DOWN RELEASE\n");
                            }
                            if (ev.number == JOY_LEFT && input_test_right == 0) {
                                lv_obj_add_flag(ui_imgTesterInputDpadLeft, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_LEFT RELEASE\n");
                            }
                            else if (ev.number == JOY_RIGHT  && input_test_right == 1) {
                                input_test_right = 0;
                                lv_obj_add_flag(ui_imgTesterInputDpadRight, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_RIGHT RELEASE\n");
                            }
                        }
                        if (ev.value == -32767) {
                            if (ev.number == JOY_UP) {
                                lv_obj_clear_flag(ui_imgTesterInputDpadUp, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_UP PRESS\n");
                            }
                            if (ev.number == JOY_LEFT) {
                                lv_obj_clear_flag(ui_imgTesterInputDpadLeft, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_LEFT PRESS\n");
                            }
                            if (ev.number == JOY_R2) {
                                lv_obj_add_flag(ui_imgTesterInputR2, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_R2 RELEASE\n");
                            }
                            if (ev.number == JOY_L2) {
                                lv_obj_add_flag(ui_imgTesterInputL2, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_L2 RELEASE\n");
                            }
                        }
                        if (ev.value == 32767) {
                            if (ev.number == JOY_DOWN) {
                                input_test_down = 1;
                                lv_obj_clear_flag(ui_imgTesterInputDpadDown, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_DOWN PRESS\n");
                            }
                            if (ev.number == JOY_RIGHT) {
                                input_test_right = 1;
                                lv_obj_clear_flag(ui_imgTesterInputDpadRight, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_RIGHT PRESS\n");
                            }
                            if (ev.number == JOY_R2) {
                                lv_obj_clear_flag(ui_imgTesterInputR2, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_R2 PRESS\n");
                            }
                            if (ev.number == JOY_L2) {
                                lv_obj_clear_flag(ui_imgTesterInputL2, LV_OBJ_FLAG_HIDDEN);
                                lv_textarea_add_text(ui_txtTesterLog, "JOY_L2 PRESS\n");
                            }
                        }
                    }
                    else if (active_screen == ui_scrCardTools && msgbox_active == 0) {
                        struct _lv_obj_t *element_focused = lv_group_get_focused(cardtools_group);
                        if (element_focused == ui_btnCardToolsFormat) {
                            lv_label_set_text(ui_lblCardToolsMessage, "X: Format as FAT32\nY: Format as EXT4");
                        }
                        else if (element_focused == ui_btnCardToolsSync) {
                            lv_label_set_text(ui_lblCardToolsMessage, "");
                        }
                        else if (element_focused == ui_btnCardToolsHealth) {
                            lv_label_set_text(ui_lblCardToolsMessage, "");
                        }
                        else if (element_focused == ui_btnCardToolsCancel) {
                            lv_label_set_text(ui_lblCardToolsMessage, "");
                        }
                    }
                    if (ev.number == JOY_LEFT && ev.value == -32767 && msgbox_active == 0) {
                        lv_refr_now(NULL);
                        //
                        // MAIN MENU
                        //
                        if (active_screen == ui_scrMainmenu) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(mainmenu_group);
                            if (element_focused == ui_btnUtilities ||
                                element_focused == ui_btnTimer ||
                                element_focused == ui_btnBackup ||
                                element_focused == ui_btnRetroArch) {
                                nav_prev(mainmenu_group, 1);
                            }
                            break;
                        }
                        //
                        // DATE AND TIME
                        //
                        else if (active_screen == ui_scrDatetime) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(datetime_group);
                            if (element_focused == ui_rolMonth ||
                                element_focused == ui_rolDay ||
                                element_focused == ui_rolHour ||
                                element_focused == ui_rolMinute ||
                                element_focused == ui_btnDatetimeSave) {
                                nav_prev(datetime_group, 1);
                            }
                            else if (element_focused == ui_rolYear) {
                                nav_prev(datetime_group, 2);
                            }
                            break;
                        }
                        //
                        // UTILITIES
                        //
                        else if (active_screen == ui_scrUtilities) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(utility_group);
                            if (element_focused == ui_btnUtilitiesExtract ||
                                element_focused == ui_btnUtilitiesSDTools ||
                                element_focused == ui_btnUtilitiesSysInfo ||
                                element_focused == ui_btnUtilitiesCredits) {
                                nav_prev(utility_group, 1);
                            }
                            break;
                        }
                        //
                        // TRACKER
                        //
                        else if (active_screen == ui_scrTracker) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(tracker_group);
                            if (element_focused == ui_btnTrackerSummary) {
                                nav_prev(tracker_group, 1);
                            }
                            break;
                        }
                        //
                        // PLAYLIST UPDATE
                        //
                        else if (active_screen == ui_scrUpdate) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(update_group);
                            if (element_focused == ui_btnUpdatePlaylist) {
                                nav_prev(update_group, 1);
                            }
                            break;
                        }
                        //
                        // CARD TOOLS
                        //
                        else if (active_screen == ui_scrCardTools) {
                            if (tf2_found == 1) {
                                struct _lv_obj_t *element_focused = lv_group_get_focused(cardtools_group);
                                if (element_focused == ui_btnCardToolsSync ||
                                    element_focused == ui_btnCardToolsHealth) {
                                    nav_prev(cardtools_group, 1);
                                }
                            }
                            break;
                        }
                        //
                        // EXTRACTION
                        //
                        else if (active_screen == ui_scrExtract) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(extract_group);
                            if (element_focused == ui_btnExtractCheats) {
                                nav_prev(extract_group, 1);
                            }
                            break;
                        }
                        //
                        // BOOT OPTIONS
                        //
                        else if (active_screen == ui_scrBootOption) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(bootoption_group);
                            if (element_focused == ui_droTheme) {
                                nav_prev(bootoption_group, 3);
                            }
                            else if (element_focused == ui_droBattery) {
                                nav_prev(bootoption_group, 2);
                            }
                            else if (element_focused == ui_btnBootSave) {
                                nav_prev(bootoption_group, 1);
                            }
                            break;
                        }
                        //
                        // SYSTEM TIMERS
                        //
                        else if (active_screen == ui_scrTimer) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(timer_group);
                            if (element_focused == ui_btnTimerSave) {
                                nav_prev(timer_group, 1);
                            }
                            break;
                        }
                        //
                        // RESET
                        //
                        else if (active_screen == ui_scrReset) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(reset_group);
                            if (element_focused == ui_btnResetConfirm) {
                                nav_prev(reset_group, 1);
                            }
                            break;
                        }
                        //
                        // BACKUP
                        //
                        else if (active_screen == ui_scrBackup) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(backup_group);
                            if (element_focused == ui_btnBackupConfirm) {
                                nav_prev(backup_group, 1);
                            }
                            break;
                        }
                        //
                        // SETUP
                        //
                        else if (active_screen == ui_scrSetup) {
                            lv_group_focus_prev(setup_group);
                            break;
                        }
                    }
                    else if (ev.number == JOY_RIGHT && ev.value == 32767 && msgbox_active == 0) {
                        lv_refr_now(NULL);
                        //
                        // MAIN MENU
                        //
                        if (active_screen == ui_scrMainmenu) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(mainmenu_group);
                            if (element_focused == ui_btnDatetime ||
                                element_focused == ui_btnBootOptions ||
                                element_focused == ui_btnReset ||
                                element_focused == ui_btnReboot) {
                                nav_next(mainmenu_group, 1);
                            }
                            break;
                        }
                        //
                        // DATE AND TIME
                        //
                        else if (active_screen == ui_scrDatetime) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(datetime_group);
                            if (element_focused == ui_rolYear ||
                                element_focused == ui_rolMonth ||
                                element_focused == ui_rolDay ||
                                element_focused == ui_rolHour ||
                                element_focused == ui_btnDatetimeCancel) {
                                nav_next(datetime_group, 1);
                            }
                            else if (element_focused == ui_rolMinute) {
                                nav_next(datetime_group, 2);
                            }
                            break;
                        }
                        //
                        // UTILITIES
                        //
                        else if (active_screen == ui_scrUtilities) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(utility_group);
                            if (element_focused == ui_btnUtilitiesTracker ||
                                element_focused == ui_btnUtilitiesTester ||
                                element_focused == ui_btnUtilitiesUpdate ||
                                element_focused == ui_btnUtilitiesCancel) {
                                nav_next(utility_group, 1);
                            }
                            break;
                        }
                        //
                        // TRACKER
                        //
                        else if (active_screen == ui_scrTracker) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(tracker_group);
                            if (element_focused == ui_btnTrackerCancel) {
                                nav_next(tracker_group, 1);
                            }
                            break;
                        }
                        //
                        // PLAYLIST UPDATE
                        //
                        else if (active_screen == ui_scrUpdate) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(update_group);
                            if (element_focused == ui_btnUpdateCancel) {
                                nav_next(update_group, 1);
                            }
                            break;
                        }
                        //
                        // CARD TOOLS
                        //
                        else if (active_screen == ui_scrCardTools) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(cardtools_group);
                            if (element_focused == ui_btnCardToolsFormat ||
                                element_focused == ui_btnCardToolsSync) {
                                nav_next(cardtools_group, 1);
                            }
                            break;
                        }
                        //
                        // EXTRACTION
                        //
                        else if (active_screen == ui_scrExtract) {
                            break;
                        }
                        //
                        // BOOT OPTIONS
                        //
                        else if (active_screen == ui_scrBootOption) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(bootoption_group);
                            if (element_focused == ui_chkSilent) {
                                nav_next(bootoption_group, 4);
                            }
                            else if (element_focused == ui_chkGamepad ||
                                element_focused == ui_chkDebug) {
                                nav_next(bootoption_group, 3);
                            }
                            else if (element_focused == ui_chkHDMI) {
                                nav_next(bootoption_group, 2);
                            }
                            else if (element_focused == ui_btnBootCancel) {
                                nav_next(bootoption_group, 1);
                            }
                            break;
                        }
                        //
                        // SYSTEM TIMERS
                        //
                        else if (active_screen == ui_scrTimer) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(timer_group);
                            if (element_focused == ui_btnTimerCancel) {
                                nav_next(timer_group, 1);
                            }
                            break;
                        }
                        //
                        // RESET
                        //
                        else if (active_screen == ui_scrReset) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(reset_group);
                            if (element_focused == ui_btnResetCancel) {
                                nav_next(reset_group, 1);
                            }
                            break;
                        }
                        //
                        // BACKUP
                        //
                        else if (active_screen == ui_scrBackup) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(backup_group);
                            if (element_focused == ui_btnBackupCancel) {
                                nav_next(backup_group, 1);
                            }
                            break;
                        }
                        //
                        // SETUP
                        //
                        else if (active_screen == ui_scrSetup) {
                            nav_next(setup_group, 1);
                            break;
                        }
                    }
                    else if (ev.number == JOY_UP && ev.value == -32767 && msgbox_active == 0) {
                        lv_refr_now(NULL);
                        //
                        // MAIN MENU
                        //
                        if (active_screen == ui_scrMainmenu) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(mainmenu_group);
                            if (element_focused == ui_btnBootOptions ||
                                element_focused == ui_btnTimer ||
                                element_focused == ui_btnReset ||
                                element_focused == ui_btnBackup ||
                                element_focused == ui_btnReboot ||
                                element_focused == ui_btnRetroArch) {
                                nav_prev(mainmenu_group, 2);
                            }
                            break;
                        }
                        //
                        // DATE AND TIME
                        //
                        else if (active_screen == ui_scrDatetime) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(datetime_group);
                            if (element_focused == ui_btnDatetimeCancel) {
                                nav_prev(datetime_group, 5);
                            }
                            else if (element_focused == ui_btnDatetimeSave) {
                                nav_prev(datetime_group, 2);
                            }
                            else if (element_focused == ui_rolYear ||
                                element_focused == ui_rolMonth ||
                                element_focused == ui_rolDay ||
                                element_focused == ui_rolHour ||
                                element_focused == ui_rolMinute) {
                                roller_element_change(element_focused, -1);
                            }
                            break;
                        }
                        //
                        // BOOT OPTIONS
                        //
                        else if (active_screen == ui_scrBootOption) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(bootoption_group);
                            if (element_focused == ui_chkGamepad ||
                                element_focused == ui_chkDebug ||
                                element_focused == ui_chkHDMI) {
                                nav_prev(bootoption_group, 1);
                            }
                            else if (element_focused == ui_droTheme) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, -1);
                                }
                            }
                            else if (element_focused == ui_droBattery) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, -1);
                                } else {
                                    nav_prev(bootoption_group, 1);
                                }
                            }
                            else if (element_focused == ui_btnBootSave) {
                                nav_prev(bootoption_group, 2);
                            }
                            else if (element_focused == ui_btnBootCancel) {
                                nav_prev(bootoption_group, 3);
                            }
                            break;
                        }
                        //
                        // SYSTEM TIMERS
                        //
                        else if (active_screen == ui_scrTimer) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(timer_group);
                            if (element_focused == ui_droBlank) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, -1);
                                }
                            }
                            else if (element_focused == ui_droShutdown ||
                                element_focused == ui_droLowBattery) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, -1);
                                } else {
                                    nav_prev(timer_group, 1);
                                }
                            }
                            else if (element_focused == ui_btnTimerCancel) {
                                nav_prev(timer_group, 1);
                            }
                            else if (element_focused == ui_btnTimerSave) {
                                nav_prev(timer_group, 2);
                            }
                            break;
                        }
                        //
                        // UTILITIES
                        //
                        else if (active_screen == ui_scrUtilities) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(utility_group);
                            if (element_focused == ui_btnUtilitiesCancel ||
                                element_focused == ui_btnUtilitiesCredits ||
                                element_focused == ui_btnUtilitiesUpdate ||
                                element_focused == ui_btnUtilitiesSysInfo ||
                                element_focused == ui_btnUtilitiesTester ||
                                element_focused == ui_btnUtilitiesSDTools) {
                                nav_prev(utility_group, 2);
                            }
                            break;
                        }
                        //
                        // TRACKER
                        //
                        else if (active_screen == ui_scrTracker) {
                            if (lv_obj_get_scroll_y(ui_pnlTrackerContent) >= 0) {
                                lv_obj_scroll_by(ui_pnlTrackerContent, 0, 50, LV_ANIM_ON);
                            }
                            break;
                        }
                        //
                        // CARD TOOLS
                        //
                        else if (active_screen == ui_scrCardTools) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(cardtools_group);
                            if (element_focused == ui_btnCardToolsCancel) {
                                if (tf2_found == 1) {
                                    nav_prev(cardtools_group, 3);
                                } else {
                                    nav_prev(cardtools_group, 1);
                                }
                            }
                            break;
                        }
                        //
                        // RESET
                        //
                        else if (active_screen == ui_scrReset) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(reset_group);
                            if (element_focused == ui_droReset) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, -1);
                                }
                            }
                            else if (element_focused == ui_btnResetCancel) {
                                nav_prev(reset_group, 1);
                            }
                            else if (element_focused == ui_btnResetConfirm) {
                                nav_next(reset_group, 1);
                            }
                            break;
                        }
                        //
                        // BACKUP
                        //
                        else if (active_screen == ui_scrBackup) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(backup_group);
                            if (element_focused == ui_chkPlaylists ||
                                element_focused == ui_chkSaves ||
                                element_focused == ui_chkSystem ||
                                element_focused == ui_btnBackupCancel) {
                                nav_prev(backup_group, 1);
                            }
                            else if (element_focused == ui_btnBackupConfirm) {
                                nav_prev(backup_group, 2);
                            }
                            break;
                        }
                        //
                        // EXTRACTION
                        //
                        else if (active_screen == ui_scrExtract) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(extract_group);
                            if (element_focused == ui_btnExtractCancel) {
                                nav_next(extract_group, 1);
                            }
                            break;
                        }
                        //
                        // SETUP
                        //
                        else if (active_screen == ui_scrSetup) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(setup_group);
                            if (element_focused == ui_droSetupBattery ||
                                element_focused == ui_droSetupCard) {
                                    if (lv_dropdown_is_open(element_focused)) {
                                        dropdown_element_change(element_focused, -1);
                                    } else {
                                        run_dropdown_seq(element_focused);
                                    }
                                }
                            break;
                        }
                    }
                    else if (ev.number == JOY_DOWN && ev.value == 32767 && msgbox_active == 0) {
                        lv_refr_now(NULL);
                        //
                        // MAIN MENU
                        //
                        if (active_screen == ui_scrMainmenu) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(mainmenu_group);
                            if (element_focused == ui_btnDatetime ||
                                element_focused == ui_btnUtilities ||
                                element_focused == ui_btnBootOptions ||
                                element_focused == ui_btnTimer ||
                                element_focused == ui_btnReset ||
                                element_focused == ui_btnBackup) {
                                nav_next(mainmenu_group, 2);
                            }
                            break;
                        }
                        //
                        // DATE AND TIME
                        //
                        else if (active_screen == ui_scrDatetime) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(datetime_group);
                            if (element_focused == ui_rolYear ||
                                element_focused == ui_rolMonth ||
                                element_focused == ui_rolDay ||
                                element_focused == ui_rolHour ||
                                element_focused == ui_rolMinute) {
                                roller_element_change(element_focused, +1);
                            }
                            break;
                        }
                        //
                        // BOOT OPTIONS
                        //
                        else if (active_screen == ui_scrBootOption) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(bootoption_group);
                            if (element_focused == ui_chkSilent ||
                                element_focused == ui_chkGamepad ||
                                element_focused == ui_chkDebug) {
                                nav_next(bootoption_group, 1);
                            }
                            else if (element_focused == ui_chkHDMI) {
                                nav_next(bootoption_group, 3);
                            }
                            else if (element_focused == ui_droTheme) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, +1);
                                } else {
                                    nav_next(bootoption_group, 1);
                                }
                            }
                            else if (element_focused == ui_droBattery) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, +1);
                                } else {
                                    nav_next(bootoption_group, 2);
                                }
                            }
                            break;
                        }
                        //
                        // SYSTEM TIMERS
                        //
                        else if (active_screen == ui_scrTimer) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(timer_group);
                            if (element_focused == ui_droBlank ||
                                element_focused == ui_droShutdown) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, +1);
                                } else {
                                    nav_next(timer_group, 1);
                                }
                            }
                            else if (element_focused == ui_droLowBattery) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, +1);
                                } else {
                                    nav_next(timer_group, 2);
                                }
                            }
                            break;
                        }
                        //
                        // UTILITIES
                        //
                        else if (active_screen == ui_scrUtilities) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(utility_group);
                            if (element_focused == ui_btnUtilitiesTracker ||
                                element_focused == ui_btnUtilitiesExtract ||
                                element_focused == ui_btnUtilitiesUpdate ||
                                element_focused == ui_btnUtilitiesSysInfo ||
                                element_focused == ui_btnUtilitiesTester ||
                                element_focused == ui_btnUtilitiesSDTools) {
                                nav_next(utility_group, 2);
                            }
                            break;
                        }
                        //
                        // TRACKER
                        //
                        else if (active_screen == ui_scrTracker) {
                            if (lv_obj_get_scroll_y(ui_pnlTrackerContent) <= 550) {
                                lv_obj_scroll_by(ui_pnlTrackerContent, 0, -50, LV_ANIM_ON);
                            }
                            break;
                        }
                        //
                        // CARD TOOLS
                        //
                        else if (active_screen == ui_scrCardTools) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(cardtools_group);
                            if (element_focused == ui_btnCardToolsHealth) {
                                nav_next(cardtools_group, 1);
                            }
                            else if (element_focused == ui_btnCardToolsSync) {
                                nav_next(cardtools_group, 2);
                            }
                            else if (element_focused == ui_btnCardToolsFormat) {
                                nav_prev(cardtools_group, 1);
                            }
                            break;
                        }
                        //
                        // RESET
                        //
                        else if (active_screen == ui_scrReset) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(reset_group);
                            if (element_focused == ui_droReset) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, +1);
                                } else {
                                    nav_next(reset_group, 1);
                                }
                            }
                            break;
                        }
                        //
                        // BACKUP
                        //
                        else if (active_screen == ui_scrBackup) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(backup_group);
                            if (element_focused == ui_chkConfigs ||
                                element_focused == ui_chkPlaylists ||
                                element_focused == ui_chkSaves ||
                                element_focused == ui_chkSystem) {
                                nav_next(backup_group, 1);
                            }
                            break;
                        }
                        //
                        // EXTRACTION
                        //
                        else if (active_screen == ui_scrExtract) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(extract_group);
                            if (element_focused == ui_btnExtractCheats) {
                                nav_next(extract_group, 1);
                            }
                            break;
                        }
                        //
                        // SETUP
                        //
                        else if (active_screen == ui_scrSetup) {
                            struct _lv_obj_t *element_focused = lv_group_get_focused(setup_group);
                            if (element_focused == ui_droSetupBattery ||
                                element_focused == ui_droSetupCard) {
                                if (lv_dropdown_is_open(element_focused)) {
                                    dropdown_element_change(element_focused, +1);
                                } else {
                                    run_dropdown_seq(element_focused);
                                }
                            }
                            break;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}

void update_datetime_placeholder(lv_obj_t *label) {
    time_t now = time(NULL);
    struct tm *timeinfo = localtime(&now);

    char datetime_str[MAX_BUFFER_SIZE];
    strftime(datetime_str, sizeof(datetime_str), "%a %b %e %Y - %I:%M %P", timeinfo);

    lv_label_set_text(label, datetime_str);
}

static void datetime_task() {
    lv_obj_t *labels[] = {
        ui_lblMainmenuDescDatetime,
        ui_lblDatetimeDescDatetime,
        ui_lblUtilitiesDescDatetime,
        ui_lblTrackerDescDatetime,
        ui_lblExtractDescDatetime,
        ui_lblUpdateDescDatetime,
        ui_lblTesterDescDatetime,
        ui_lblCardToolsDescDatetime,
        ui_lblSysInfoDescDatetime,
        ui_lblCreditsDescDatetime,
        ui_lblBootOptionDescDatetime,
        ui_lblTimerDescDatetime,
        ui_lblResetDescDatetime,
        ui_lblBackupDescDatetime,
        ui_lblSetupDescDatetime
    };

    while (1) {
        for (size_t i = 0; i < sizeof(labels) / sizeof(labels[0]); i++) {
            update_datetime_placeholder(labels[i]);
        }

        sleep(20);
    }
}

static void set_bar_styles(lv_obj_t* bar) {
    lv_obj_set_style_bg_color(bar, UI_BAR_COLOR, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(bar, 0, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_set_style_border_color(bar, UI_BAR_BORDER_COLOR, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_pad_left(bar, UI_BAR_PADDING, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_pad_right(bar, UI_BAR_PADDING, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_pad_top(bar, UI_BAR_PADDING, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_pad_bottom(bar, UI_BAR_PADDING, LV_PART_MAIN | LV_STATE_DEFAULT);
}

static void progress_task() {
    int value = 10;
    int start = 0;
    int direction = 0;
    
    set_bar_styles(ui_barExtractProgress);
    set_bar_styles(ui_barTrackerProgress);
    
    lv_bar_set_mode(ui_barExtractProgress, LV_BAR_MODE_RANGE);
    lv_bar_set_mode(ui_barTrackerProgress, LV_BAR_MODE_RANGE);

    while (1) {
        lv_bar_set_value(ui_barExtractProgress, value, LV_ANIM_OFF);
        lv_bar_set_start_value(ui_barExtractProgress, start, LV_ANIM_OFF);

        lv_bar_set_value(ui_barTrackerProgress, value, LV_ANIM_OFF);
        lv_bar_set_start_value(ui_barTrackerProgress, start, LV_ANIM_OFF);

        if (direction == 0) {
            value += 10;
            start += 10;
            if (value >= 100) {
                direction = 1;
            }
        } else if (direction == 1) {
            value -= 10;
            start -= 10;
            if (value <= 10) {
                direction = 0;
            }
        }

        usleep(500000);
    }
}

static void turbo_task() {
    while (1) {
        if (turbo_mode == 1) {
            set_governor("performance");
            set_cpu_scale(1388000);
        } else {
            set_governor("powersave");
            set_cpu_scale(388000);
        }
        sleep(1);
    }
}

int main(int argc, char *argv[]) {
    setenv("PATH", "/bin:/usr/bin:/system/bin", 1);
    setenv("NO_COLOR", "1", 1);

    lv_init();
    fbdev_init();

    static lv_color_t buf[DISP_BUF_SIZE];
    static lv_disp_draw_buf_t disp_buf;

    lv_disp_draw_buf_init(&disp_buf, buf, NULL, DISP_BUF_SIZE);

    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.draw_buf = &disp_buf;
    disp_drv.flush_cb = fbdev_flush;
    disp_drv.hor_res = 640;
    disp_drv.ver_res = 480;
    lv_disp_drv_register(&disp_drv);

    ui_init();

    init_navigation_groups();
    elements_events_init();

    if (load_config(&options, &num_options) != 0) {
        perror("Failed to load configuration");
        return 1;
    }

    js_fd = open("/dev/input/js0", O_RDONLY);
    if (js_fd < 0) {
        perror("Failed to open joystick device");
        return 1;
    }

    pthread_t joystick_thread;
    pthread_create(&joystick_thread, NULL, (void*(*)(void*))joystick_task, NULL);

    pthread_t datetime_thread;
    pthread_create(&datetime_thread, NULL, (void*(*)(void*))datetime_task, NULL);

    pthread_t progress_thread;
    pthread_create(&progress_thread, NULL, (void*(*)(void*))progress_task, NULL);

    pthread_t turbo_thread;
    pthread_create(&turbo_thread, NULL, (void*(*)(void*))turbo_task, NULL);

    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);

    indev_drv.type = LV_INDEV_TYPE_KEYPAD;
    indev_drv.read_cb = evdev_read;
    indev_drv.user_data = (void*)(intptr_t)js_fd;

    lv_indev_drv_register(&indev_drv);

    int a;
    for (a = 1; a < argc; a++) {
        if (strcmp(argv[a], "--setup") == 0) {
            lv_scr_load(ui_scrSetup);
        } else {
            lv_scr_load(ui_scrMainmenu);
        }

        if (strcmp(argv[a], "--debug") == 0) {
            debug = 1;
        }
    }

    active_screen = lv_scr_act();

    while (1) {
        lv_task_handler();
        usleep(2500);
    }

    pthread_cancel(joystick_thread);
    close(js_fd);

    return 0;
}

uint32_t custom_tick_get(void) {
    static uint64_t start_ms = 0;

    if (start_ms == 0) {
        struct timeval tv_start;
        gettimeofday(&tv_start, NULL);
        start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
    }

    struct timeval tv_now;
    gettimeofday(&tv_now, NULL);

    uint64_t now_ms;
    now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;

    uint32_t time_ms = now_ms - start_ms;
    return time_ms;
}
