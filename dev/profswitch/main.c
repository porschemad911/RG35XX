#include "lvgl/lvgl.h"
#include "lv_drivers/display/fbdev.h"
#include "lv_drivers/indev/evdev.h"
#include "lvgl/src/core/lv_event.h"
#include "lvgl/src/core/lv_group.h"
#include "lvgl/src/core/lv_obj.h"
#include "lvgl/src/misc/lv_anim.h"
#include "lvgl/src/misc/lv_style.h"
#include "lvgl/src/widgets/lv_label.h"
#include "ui.h"
#include <regex.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <sys/mount.h>
#include <sys/reboot.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/vfs.h>
#include <linux/input.h>
#include <linux/joystick.h>
#include <zip.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define JOY_A		0
#define JOY_MENU	9
#define JOY_LEFT	6
#define JOY_RIGHT	6

#define MAX_BUFFER_SIZE 256
#define DISP_BUF_SIZE	(640 * 480 * 32) / 8

static int js_fd;
static lv_obj_t* active_screen = NULL;
static lv_group_t* profile_group;

char title[24];
char col_title[8];
char col_back[8];
char col_select[8];
char col_datetime[8];

struct Profile {
    char name[16];
    char image[16];
    char colour[8];
    int disabled;
    int hidden;
    char command[256];
};
struct Profile profile[3];

void parse_title(FILE *file) {
    char line[256];
    fgets(line, sizeof(line), file);
    char *value = strtok(line, "=");
    if (value != NULL) {
        value = strtok(NULL, "\n");
        if (value != NULL) {
            strcpy(title, value);
        }
    }
}

void parse_colour(FILE *file, char *name, char *value) {
    char line[256];
    fgets(line, sizeof(line), file);
    char *token = strtok(line, "=");
    if (token != NULL) {
        char *value = strtok(NULL, "\n");
        if (value != NULL) {
            if (strcmp(token, name) == 0) {
                strcpy(title, value);
            }
        }
    }
}

void parse_profile(FILE *file, struct Profile *profile, int number) {
    char line[256];
    fgets(line, sizeof(line), file);
    char *token = strtok(line, "=");
    if (token != NULL) {
        char *value = strtok(NULL, "\n");
        if (value != NULL) {
            struct Profile *profile = &profile[number];
            if (strcmp(token, "NAME") == 0) {
                strcpy(profile->name, value);
            } else if (strcmp(token, "IMAGE") == 0) {
                strcpy(profile->image, value);
            } else if (strcmp(token, "COLOUR") == 0) {
                strcpy(profile->colour, value);
            } else if (strcmp(token, "DISABLED") == 0) {
                profile->disabled = atoi(value);
            } else if (strcmp(token, "HIDDEN") == 0) {
                profile->hidden = atoi(value);
            } else if (strcmp(token, "COMMAND") == 0) {
                strcpy(profile->command, value);
            }
        }
    }
}

void nav_prev(lv_group_t* group, int count) {
    int i;
    for (i = 0; i < count; i++) {
        lv_group_focus_prev(group);
    }
}

void nav_next(lv_group_t* group, int count) {
    int i;
    for (i = 0; i < count; i++) {
        lv_group_focus_next(group);
    }
}

void init_navigation_groups() {
    lv_obj_t* profile_objects[] = {
        ui_pnlProfileOne,
        ui_pnlProfileTwo,
        ui_pnlProfileThree
    };

    profile_group = lv_group_create();

    for (int i = 0; i < sizeof(profile_objects) / sizeof(profile_objects[0]); i++) {
        lv_group_add_obj(profile_group, profile_objects[i]);
    }
}

static void joystick_task() {
    struct js_event ev;

    while (1) {
        read(js_fd, &ev, sizeof(struct js_event));
        switch (ev.type) {
            case JS_EVENT_BUTTON:
                if (ev.number == JOY_A && ev.value == 1) {
                    if (active_screen == ui_scrProfile) {
                        struct _lv_obj_t *element_focused = lv_group_get_focused(profile_group);
                        if (element_focused == ui_pnlProfileOne) {
                            system(profile[0].command);
                            break;
                        }
                        else if (element_focused == ui_pnlProfileTwo) {
                            system(profile[1].command);
                            break;
                        }
                        else if (element_focused == ui_pnlProfileThree) {
                            system(profile[2].command);
                            break;
                        }
                        break;
                    }
                }
                break;
            case JS_EVENT_AXIS:
                if (ev.number == JOY_LEFT && ev.value == -32767) {
                    lv_refr_now(NULL);
                    if (active_screen == ui_scrProfile) {
                        nav_prev(profile_group, 1);
                        break;
                    }
                }
                else if (ev.number == JOY_RIGHT && ev.value == 32767) {
                    lv_refr_now(NULL);
                    if (active_screen == ui_scrProfile) {
                        nav_next(profile_group, 1);
                        break;
                    }
                }
                break;
            default:
                break;
        }
    }
}

void update_datetime_placeholder(lv_obj_t *label) {
    time_t now = time(NULL);
    struct tm *timeinfo = localtime(&now);

    char datetime_str[128];
    strftime(datetime_str, sizeof(datetime_str), "%a %b %e %Y - %I:%M %P", timeinfo);

    lv_label_set_text(label, datetime_str);
}

static void datetime_task() {
    lv_obj_t *labels[] = {
        ui_lblProfileDatetime
    };

    while (1) {
        for (size_t i = 0; i < sizeof(labels) / sizeof(labels[0]); i++) {
            update_datetime_placeholder(labels[i]);
        }

        sleep(20);
    }
}

int main(int argc, char *argv[]) {
    FILE *file = fopen("config.txt", "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    setenv("PATH", "/bin:/usr/bin:/system/bin", 1);
    setenv("NO_COLOR", "1", 1);

    parse_colour(file, "COL_TITLE", col_title);
    parse_colour(file, "COL_BACK", col_back);
    parse_colour(file, "COL_SELECT", col_select);
    parse_colour(file, "COL_DATETIME", col_datetime);

    parse_title(file);
    parse_profile(file, profile, 0);
    parse_profile(file, profile, 1);
    parse_profile(file, profile, 2);

    // testing testing one two three! :D
    printf("Main:\n");
    printf("\tTitle: %s\n", title);
    printf("\tColour Title: %s\n", col_back);
    printf("\tColour Background: %s\n", col_back);
    printf("\tColour Selection: %s\n", col_select);
    printf("\tColour Datetime: %s\n", col_datetime);
    printf("\n");

    for (int i = 0; i < 3; i++) {
        printf("Profile %d:\n", i + 1);
        printf("\tName: %s\n", profile[i].name);
        printf("\tImage: %s\n", profile[i].image);
        printf("\tColour: %s\n", profile[i].colour);
        printf("\tDisabled: %d\n", profile[i].disabled);
        printf("\tHidden: %d\n", profile[i].hidden);
        printf("\tCommand: %s\n", profile[i].command);
        printf("\n");
    }
    // testing has ended :(

    lv_init();
    fbdev_init();

    static lv_color_t buf[DISP_BUF_SIZE];
    static lv_disp_draw_buf_t disp_buf;

    lv_disp_draw_buf_init(&disp_buf, buf, NULL, DISP_BUF_SIZE);

    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.draw_buf = &disp_buf;
    disp_drv.flush_cb = fbdev_flush;
    disp_drv.hor_res = 640;
    disp_drv.ver_res = 480;
    lv_disp_drv_register(&disp_drv);

    ui_init();

    init_navigation_groups();

    js_fd = open("/dev/input/js0", O_RDONLY);
    if (js_fd < 0) {
        perror("Failed to open joystick device");
        return 1;
    }

    pthread_t joystick_thread;
    pthread_create(&joystick_thread, NULL, (void*(*)(void*))joystick_task, NULL);

    pthread_t datetime_thread;
    pthread_create(&datetime_thread, NULL, (void*(*)(void*))datetime_task, NULL);

    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);

    indev_drv.type = LV_INDEV_TYPE_KEYPAD;
    indev_drv.read_cb = evdev_read;
    indev_drv.user_data = (void*)(intptr_t)js_fd;

    lv_indev_drv_register(&indev_drv);

    lv_scr_load(ui_scrProfile);

    active_screen = lv_scr_act();

    while (1) {
        lv_task_handler();
        usleep(2500);
    }

    pthread_cancel(joystick_thread);
    pthread_cancel(datetime_thread);

    close(js_fd);
    fclose(file);

    return 0;
}

uint32_t custom_tick_get(void) {
    static uint64_t start_ms = 0;

    if (start_ms == 0) {
        struct timeval tv_start;
        gettimeofday(&tv_start, NULL);
        start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
    }

    struct timeval tv_now;
    gettimeofday(&tv_now, NULL);

    uint64_t now_ms;
    now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;

    uint32_t time_ms = now_ms - start_ms;
    return time_ms;
}
