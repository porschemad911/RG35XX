# Spring-LibRetro Launcher

Launch any command through RetroArch.

This is a modified adaptation of [spring-libretro by christianhaitian](https://github.com/christianhaitian/spring-libretro/).

# Install

Copy `spring_libretro.info` to a unique name such as `spring_bingbong_libretro.so` in your cores directory.

Using your favourite text editor open `spring_bingbong_libretro.info` and fill in the details including extensions for matching.

Add your command to execute. %s will be replaced with the file chosen.

Run `make` and it will generate .so cores for each of the individual .info files it finds.
