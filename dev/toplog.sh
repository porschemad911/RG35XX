#!/system/bin/sh

LOGFILE="/mnt/mmc/process.log"
LOOP=60
HOLD=1

echo "Timestamp | CPU Usage (%) | Process CPU Usage (%)" > "$LOGFILE"

I=0
while [ "$I" -lt "$LOOP" ]; do
    I=$((I + 1))
    DATETIME=$(busybox date +"%Y-%m-%d %H:%M:%S")
    USAGE_CPU=$(busybox top -b -n 1 | busybox awk '/%Cpu/{print $2}')
    USAGE_PROC=$(busybox top -b -n 1 | busybox awk '/PID/{getline; print $9}')

    echo "$DATETIME | $USAGE_CPU | $USAGE_PROC" >> "$LOGFILE
"
    sleep "$HOLD"
done &
