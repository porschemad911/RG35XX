## RG35XX Modifications

All of the following mods should work on GarlicOS. These mods come standard on muOS.

* `battery_warning` - To alert the user their battery is getting low.
* `boot_animation` - Allow the user to have an animated boot with sound and rumble.
* `charger_images` - Customise the battery charging images.

Ready downloads are at: https://archive.xonglebongle.com/
