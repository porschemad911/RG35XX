---
title: Change Log
description: A list of all the developmental changes to make muOS a great CFW.
hide:
    - toc
---

???+ success "muOS 2308u-pr4 - 27 August 2023"
    #### System Changes
    * Theme structure has changed
        - Added new 'rainyday' theme
        - Place battery and boot images into the appropriate folder
        - Factory reset and update music can be changed by placing a `reset.mp3` in a theme folder
    * Added homebrew RetroArch database
    * Added new debugging process to track CPU usage, see `options.txt` to toggle
    * Modified `JoyAudio` and `JoyBright` to use epoll and threaded tasks
        - _Still an issue where sound glitches may occur, this is being looked into!_
        - Headphone volume can now be adjusted without turning on the device speaker
    * Added 256MB swap file partition
    * Fixed issues with animation starting when reset or updating
    * Fixed issues relating to processes taking up CPU time
        - _Please wait for the starfield screensaver to start and if it lags let me know ASAP!_
    #### RetroArch Updates
    * Moved RetroArch back to `userspace` for menu governor and `performance` as default for core governor
        - Also adjusted CPU frequencies to MIN 288 and MAX 1488
    * Enabled 'Automatic Frame Delay' to reduce latency and vsync issues
        - Adjusted popular cores to use this feature
    * Remove internal volume adjustments
    #### muOS Extra Update
    * The files `mame.json` and `coremapping.json` have been move to `MUOS` folder.
    * Playlist importer now uses 'fake' CRC32 checksums for faster generation
    * Fixed system information uptime message
    #### Extra Notes
    * Shuffle is broken... again!
    * Port launchers are being redesigned
    * Boot option to start on a random game will be coming soon!
    * Gamepad and HDMI are not working despite being available in muOS Extras
    * muOS Extras still has a little bit work left to do. So some features may not be present

??? abstract "muOS 2308u-pr3 - 22 August 2023"
    #### System Changes
    * GarlicOS ROM Structure Compatibility!
        - You can now use muOS Extras to generate RetroArch playlists based on the GarlicOS ROM structure
        - This is especially handy for those running a dual SD card setup so now you have best of both worlds
    * Auto Brightness modification added
        - It will reduce brightness by a factor of 1.1 starting at 2200 hours (10:00PM)
        - The interface to change settings will come at a later update
        - _Don't forget to set your clock appropriately!_
    * Updated RetroArch cores thanks to @xquader :pray:
    #### muOS Extra Update
    * Removed options in extraction that are no longer required
    * Added 'Playlist Update' in the utilities screen for the GarlicOS ROM Structure Compatibility feature
        - Please be mindful that arcade games are slower than other systems as they need to reference a large mame.json file
    #### Extra Notes
    * Port launchers are being redesigned
    * Boot option to start on a random game will be coming soon!
    * Deciding whether or not this system requires a 256MB swap file...?
    * Gamepad and HDMI are not working despite being available in muOS Extras
    * muOS Extras still has a little bit work left to do. So some features may not be present

??? abstract "muOS 2308u-pr2 - 17 August 2023"
    #### System Changes
    * RetroArch root environment has been moved to SYSTEM partition
    * Added extra black DTB files for those who want extra contrast on screen
    * Fixed prompt for adb shell
    #### muOS Extra Update
    * Moved to SYSTEM for easier development
    * Fixed boot option file editing
    * Final (_hopefully..._) fix for the extraction and backup functions
    * `jq` added to SYSTEM for muOS Extra application
    * Moved boot animation to separate process
        - This may be faster I'm not quite sure...
    #### Extra Notes
    * Deciding whether or not this system requires a 256MB swap file...?
    * Gamepad and HDMI are not working despite being available in muOS Extras
    * TBSgo playlist assumes that ROMs are on `sdcard` not `mmc`. _This will be fixed in a later release!_
    * There is an `extra` folder within the `MUOS` directory for the extraction files
    * muOS Extras still has a little bit work left to do. So some features may not be present

??? abstract "muOS 2308u-pr1 (Update 1) - 13 August 2023"
    #### muOS Extras Update
    This is an update package for the `2308u-pr1` image only!

    **Place this update package `update.7z` into the `muOS BOOT` drive, your device will pick this up and update it automatically.**
    #### Update Package Changes
    * Updated system utilities from Buildroot
    * Fixed extraction functions
        - _Homebrew and Cheat extraction does nothing for now_
        - Will need to swap out homebrew package as it is now installed by default
    * Fixed activity tracker results
        - Should also load faster due to temporary performance mode

??? abstract "muOS 2308u-pr1 - 12 August 2023"
    #### System Changes
    * First partition has been bumped up to 128MB for future updates
    * Update system has had a makeover with scripting support
    * JoySleep and Screenshot programs have been updated to use 25% less CPU (_using epoll_)
    * Fun pre-release boot screen!
    #### muOS Extra Update
    * Fixed checkbox crashing in boot options (my bad!)
    * Fixed navigation in reset screen
    #### RetroArch Updates
    * **19 Homebrew games are now included by default**
        - _Check them out, see what you think!_
    * Shuffle mode has been fixed and is faster to choose a game!
    * Adjusted default governor to `userspace` for both core and menu
    * Explore mode is back but it seems that ROMs have to match CRC in database...
    #### Extra Notes
    * Gamepad and HDMI are not working despite being available in muOS Extras
    * Extraction seems to not work on certain instances
    * TBSgo playlist assumes that ROMs are on `sdcard` not `mmc`. This will be fixed in a later release!
    * There is an `extra` folder within the `MUOS` directory for the extraction files
    * muOS Extras still has a little bit work left to do. So some features may not be present

??? abstract "muOS 2307s-pr4 (Update 1) - 30 July 2023"
    #### RetroArch Thumbnail Update
    This is an update package for the `2307s-pr4` image only!

    **Place this update package `update.7z` into the `muOS BOOT` drive, your device will pick this up and update it automatically.**
    #### Update Package Changes
    * Explore is back, back again, tell a friend
        - This means you can once again search for ROMs
        - And also create custom lists within RetroArch
    * ROM Thumbnails are now complete wallpapers
        - Create `PNG` files and save them with the ROM filename under `IMGS\<playlist name>`
        - Images should be 320x240 in dimension
        - It's always good to create some type of gradient image too
        - Transparency will show up as black, use that to your advantage
    * Streamlined RetroArch settings menu
        - Cleaned up options that are useless on this device
        - Moved single items in sub-menus to parent menu
        - _There are still a number of menus and options to clean up!_
    * Removed `...` string at the end of menu options (just looks nicer!)
    * Added 'muOS Default' theme and removed custom theme support
        - _There are still a wide number of themes you can change to though!_
    * CPU Governor is set to `userspace` as default for cores
    * This update will also update the update system to support update scripts!
    * Shuffle mode is still broken (_we knew that already..._)
    * This update includes the fantastic homebrew game `Apotris`
        - Also includes ROM wallpaper as an example!
    * Updated `gpsp` and `dosbox-pure` cores thanks to @xquader

??? abstract "muOS 2307s-pr4 - 25 July 2023"
    _Reminder that this is a pre-release and is rough around the edges_
    #### muOS Extra Update
    * Added Activity Tracker _(work in progress)_
    * Added package extraction _(work in progress)_
    * Navigation now uses full D-pad
    * Removed manual ROM scanner
    * Changed `rsync` to standard `cp` for bios sync
    * Removed left over ROM Scanner screen code
    * Changed 'Update' to 'Summary' in Activity Tracker screen
    * Removed unused spinbox UI helper
    * Added simple nav_prev/next for proper navigation
    * Changed default governor to powersave
    * Grouped navigation functions
    * Fixed boot theme selection
    * _May_ have solved the issue with button highlights going missing
    #### RetroArch Updates
    * Adjusted CPU to: MIN 388MHz to MAX 1388MHz
    * Content import now auto redirects to manual import
    * Better video filtering support
    * Moved 'Playlists' to top of menu
    * Add Manual Scan to playlist menu
    * Removed `fbneo` built-in cheats as this was slowing load times
    * Default wallpapers created in `WALL` for various systems
    * There are now only 13 cores for this system, more can be found in `MUOS/extra/core.7z`
        * _Do we actually need all of these cores??_
    * Cores are now configured properly with some using built-in filters as standard
    #### Extra Notes
    * Gamepad and HDMI are not working despite being available in muOS Extras
    * Extraction seems to not work on certain instances
    * TBSgo playlist assumes that ROMs are on `sdcard` not `mmc`. This will be fixed in the next release
    * There is an `extra` folder within the `MUOS` directory for the extraction files
    * muOS Extras still has a little bit work left to do. So some features may not be present
    * Shuffle mode does not work. This will need to be rewritten and is a low priority at the moment
    * Website with information is coming soon with full description of muOS Extras and other upcoming features

??? abstract "muOS 2305r (p3) - 19 May 2023"
    * Simplified boot options
        - See `options.txt` file in the BOOT drive
    * Added **JoyIdle** program
        - Detection of idle input. Screen will go blank and audio muted after 5 minutes (default)
        - Device will shut down after 10 minutes (default)
        - This will **not** save your game if you are playing one so be warned!
        - Timing can be changed in the `options.txt` file mentioned above

??? abstract "muOS 2305r (p2) - 18 May 2023"
    * Boot partition moved to FAT32 file system (was FAT16)
    * Fixed factory reset music and changed to free audio
        - Art of Silence v2 by Uniq (4:13)
    * Added progress bar for factory reset process
    * Updated RetroArch cores
    * Fixed low battery indicator flashing too early
    * Modified the boot animation to be more built-in
        - Brand new boot chimes (will randomly choose out of 6 on each reboot)
    * Updated backend applications (not much to see here)
    * Fixed selected indicator symbol in RGUI
    * MENU button is now the quick access key
    * SELECT is now the default hotkey for everything
    * JoyAudio and JoyBright programs are introduced
        - These programs are system wide (Not restricted to RetroArch)
        - Settings are saved to individual files and restored upon reboot
        - **JoyAudio**
            - Audio is adjusted via ALSA not by RetroArch
            - Use normal volume buttons to change volume
            - Mute by pressing SELECT+DOWN
            - Muting or audio to 0% will switch the speaker off (no crackling)
        - **JoyBright**
            - Brightness is adjusted on a hardware level not overlay
            - Can reach low levels great for night time playing
            - Adjust by holding SELECT and volume buttons
    * Tiny little easter egg placed somewhere...

??? abstract "muOS 2305r - 12 May 2023"
    * RetroArch updated to 1.15
    * Boot animation support
        - Redesigned boot chime
        - Look at `boot_anim` inside `BOOT` for more info
    * Low battery notification
        - Set to screen blank as default for 25% battery
        - Look at `lowbatt.sh` inside `BOOT` for more info
    * Redesigned RGUI interface
        - Modified menu items for easier navigation
        - Text scaling now takes up entire screen
        - Text lines have been spaced for easier reading
    * New image support
        - Basic thumbnail support (110x110 PNG)
            - Use RetroArch thumbnail folders in LOGO directory
            - Delay has been set to reduce flickering while scrolling
        - Playlist background support (See WALL directory)
    * Idle screensaver for screen blanking
        - This is similar to menu particles (which have been removed)
        - Default is set to 60 seconds idle

??? abstract "muOS 2304p - 17 April 2023"
    * Refreshed muOS logos (thanks qpla!).
    * Modified ramdisk to reduce boot overhead and reduced boot time by ~2 seconds.
    * Symlinked battery images to /misc/battery directory.
    * Easy-to-use updating system. Place the UPDATE.7z file into BOOT to update.
        - This image should _technically_ be the last full image required moving forward.
    * PORT support, place ports inside PORT directory.
        - Use the Import Content in RetroArch and use the Port Launcher core.
    * Multiple argument support in BOOT partition.
        - `debugMe`	:: This will freeze the boot process, only used in extreme cases!
        - `enableADB` :: Enables the ADB protocol so you can adb shell to the device. 
        - `expandSDCard` :: This is only invoked during 'firstBoot' and should not be created manually.
        - `factoryReset` :: If you fuck up you can use this instead of writing the image again.
        - `firstBoot` :: This is required upon writing the 'muos.img' file to the SD card.
        - `silentBoot` :: Disables the boot chime and motor rumble upon booting the device.
    * Sounds for the boot chime and SD Card expansion.
        - To change the boot chime and expanding music you will need to play the following files into the BOOT partition.
            - `boot_chime.wav`
            - `sd_expand.wav`
        - If those files are found it will play those instead of the internal ones.
        - The sound files need to be configured with the following settings:
            - `44100Hz 16bit PCM Mono`
    * Huge thanks to poetry and qpla for their patience, ideas, and testing!

??? abstract "muOS 2304o - 9 April 2023"
    * Auto expanding SD Card on first boot.

??? abstract "muOS 2304m - 7 April 2023"
    * Fixed a bug with the volume percentage.
    * Fixed volume bug with certain core configs.
    * Added CPU hotplug script from GarlicOS.
    * Modified boot screen.
    * Added screens for shuffling and charging.
    * Initial backend support for ports are done.

??? abstract "muOS 2304k - 5 April 2023"
    * SHUFFLE Mode is now available, you'll need some ROMs to get started.
    * SD Card Image release, reduced partition sizes.
    * Remove unnecessary partitions, and files in SYSTEM.
    * Hidden SYSTEM partition due to Windows being annoying about EXT4 partitions.

??? abstract "muOS 2304e - 3 April 2023"
    * Favourites is now part of the main menu.
    * Speaking of the main menu, it will display the muOS version.
    * Removed a lot of RetroArch options that were deemed unnecessary.
    * Reorganised a few options to make it more “streamlined”.
    * HDMI support is removed for now, this is strictly a handheld system for me.
    * Volume system is now displayed as a percentage ranging from -30.0dB to 15.0dB.

??? abstract "muOS 2303c - 29 March 2023"
    * CPU Frequency and Governor selector is now available in power management settings.
    * Modified PicoScaler video filter is set as default.
    * 44100Hz Sound + LowPass filter is set as default.

??? abstract "muOS 2303a - 19 March 2023 (Unreleased)"
    * Custom RetroArch compilation.
    * RG35XX toolchain system for cross-compilation completed.
