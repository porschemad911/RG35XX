---
title: Arcade (MAME)
description: What the muOS CFW currently uses to run Arcade (MAME) ROMs.
hide:
  - toc
---

Arcade games are quite nice to play on the RG35XX however some cores play better than others.

In most cases `km_mame2003_xtreme_libretro.so` is usually the best one to choose from the other arcade cores.

> MAME as it was in 2003, with a 2K22 touch up! Suitable for lower-spec devices that would struggle to run current versions of MAME (later versions of MAME are increasingly accurate, thus can perform worse).
>
> [https://github.com/KMFDManic/mame2003-xtreme](https://github.com/KMFDManic/mame2003-xtreme)

## Configuration

The following should be saved as `MAME 2003-Xtreme.opt` in the same folder name under `.retroarch/config`.

```
mame2003-xtreme-cheats = "disabled"
mame2003-xtreme-dcs-speedhack = "enabled"
mame2003-xtreme-dialsharexy = "disabled"
mame2003-xtreme-mouse_device = "disabled"
mame2003-xtreme-oc = "86"
mame2003-xtreme-option_tate_mode = "disabled"
mame2003-xtreme-rstick_to_btns = "enabled"
mame2003-xtreme-sample_rate = "44100"
mame2003-xtreme-samples = "enabled"
mame2003-xtreme-skip_disclaimer = "enabled"
mame2003-xtreme-skip_warnings = "enabled"
mame2003-xtreme-turboboost = "20"
mame2003-xtreme-use_artwork = "enabled"
```