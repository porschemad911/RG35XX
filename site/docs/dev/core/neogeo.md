---
title: Neo Geo
description: What the muOS CFW currently uses to run Neo Geo ROMs.
hide:
  - toc
---

Depending on your take, the Neo Geo system was one of the **most** expensive home consoles money could buy.  I don't know anybody personally who owned one in my childhood, I still don't know anybody who owns one!

There are two variations to run this core; **MVS** - Arcade Cabinet _or_ **AES** - Home Console. In my configuration I've opted for AES. With the configuration you'll find that most of the dipswitches can be changed from within the RetroArch menu and will be saved alongside the core options.

This system has the most beautifully rendered and animated games you'll see on the RG35XX.

## Configuration

The following should be saved as `FinalBurn Neo.opt` in the same folder name under `.retroarch/config`.

```
fbneo-allow-depth-32 = "enabled"
fbneo-allow-patched-romsets = "enabled"
fbneo-analog-speed = "100%"
fbneo-cpu-speed-adjust = "100%"
fbneo-cyclone = "enabled"
fbneo-diagnostic-input = "None"
fbneo-fixed-frameskip = "0"
fbneo-fm-interpolation = "4-point 3rd order"
fbneo-force-60hz = "enabled"
fbneo-frameskip-manual-threshold = "33"
fbneo-frameskip-type = "disabled"
fbneo-hiscores = "enabled"
fbneo-lightgun-crosshair-emulation = "always show"
fbneo-lowpass-filter = "enabled"
fbneo-memcard-mode = "per-game"
fbneo-neogeo-mode = "AES_EUR"
fbneo-sample-interpolation = "4-point 3rd order"
fbneo-samplerate = "44100"
fbneo-vertical-mode = "disabled"
```