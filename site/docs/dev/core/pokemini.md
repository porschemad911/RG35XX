---
title: Pokémon Mini
description: What the muOS CFW currently uses to run Pokémon Mini ROMs.
hide:
  - toc
---

This one would have gone into the Nintendo section, but I just couldn't pass up placing it on it's own because of that lovely little **é** character.

It uses the pokemini core, there are only a handful of games for this one. Most of them are quite enjoyable. Just like most of the cores you'll need to place the right files in the `BIOS` directory.

## Configuration

The following should be saved as `PokeMini.opt` in the same folder name under `.retroarch/config`.

```
pokemini_60hz_mode = "enabled"
pokemini_lcdbright = "0"
pokemini_lcdcontrast = "48"
pokemini_lcdfilter = "dotmatrix"
pokemini_lcdmode = "analog"
pokemini_lowpass_filter = "enabled"
pokemini_lowpass_range = "60"
pokemini_palette = "LEDBacklight"
pokemini_piezofilter = "enabled"
pokemini_rumble_lv = "10"
pokemini_screen_shake_lv = "3"
pokemini_turbo_period = "18"
pokemini_video_scale = "4x"
```