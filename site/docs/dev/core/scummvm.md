---
title: ScummVM
description: What the muOS CFW currently uses to run ScummVM ROMs.
hide:
  - toc
---

Point and click adventure games were my go to games as a child. I've always loved a good puzzle and mix that with adventure games and everybody wins. My favourite of all time would have to be Day of The Tentacle, the humor is just on point.

There is a bit of a trick getting these to work however. Each game will need to go into their own respective directory. Within that directory should be a file of the same directory name ending in `.scummvm`. For instance, I have "The Dig" as a directory, and inside that I have `The Dig.scummvm` and the contents of that file contain the scummvm ID for that game to be detected, in this case it's `dig`.

You can find a list of ids from the following URL, keep in mind that you'll need to use the text that is **AFTER** the colon, for example `scumm:dig`. You can work that one out, it's not rocket science.

[https://www.scummvm.org/compatibility/DEV/](https://www.scummvm.org/compatibility/DEV/)

## Configuration

The following should be saved as `scummvm.opt` in the same folder name under `.retroarch/config`.

```
scummvm_analog_deadzone = "15"
scummvm_analog_response = "quadratic"
scummvm_gamepad_cursor_speed = "3.0"
scummvm_mouse_speed = "3.0"
scummvm_speed_hack = "enabled"
```