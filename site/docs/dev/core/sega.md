---
title: Sega Systems
description: What the muOS CFW currently uses to run Sega System ROMs.
hide:
  - toc
---

Most if not all of the Sega Systems will run quite nicely under the Genesis Plus GX (NOT Wide) core. With a few minor configuration edits I have yet to come across a game that does not work.

These systems seem to be the least troublesome when it comes to emulation.

_Is it see-ga, or say-ga?..._

## Configuration - SMS/MD/CD/SG

The following should be saved as `Genesis Plus GX.opt` in the same folder name under `.retroarch/config`.

```
genesis_plus_gx_add_on = "sega/mega cd"
genesis_plus_gx_addr_error = "enabled"
genesis_plus_gx_aspect_ratio = "auto"
genesis_plus_gx_audio_filter = "low-pass"
genesis_plus_gx_bios = "disabled"
genesis_plus_gx_blargg_ntsc_filter = "disabled"
genesis_plus_gx_bram = "per bios"
genesis_plus_gx_cd_latency = "enabled"
genesis_plus_gx_cdda_volume = "100"
genesis_plus_gx_enhanced_vscroll = "enabled"
genesis_plus_gx_enhanced_vscroll_limit = "16"
genesis_plus_gx_fm_preamp = "100"
genesis_plus_gx_force_dtack = "enabled"
genesis_plus_gx_frameskip = "disabled"
genesis_plus_gx_frameskip_threshold = "33"
genesis_plus_gx_gg_extra = "disabled"
genesis_plus_gx_gun_cursor = "enabled"
genesis_plus_gx_gun_input = "lightgun"
genesis_plus_gx_invert_mouse = "disabled"
genesis_plus_gx_lcd_filter = "disabled"
genesis_plus_gx_left_border = "left & right borders"
genesis_plus_gx_lock_on = "disabled"
genesis_plus_gx_lowpass_range = "60"
genesis_plus_gx_no_sprite_limit = "enabled"
genesis_plus_gx_overclock = "100%"
genesis_plus_gx_overscan = "disabled"
genesis_plus_gx_pcm_volume = "100"
genesis_plus_gx_psg_preamp = "150"
genesis_plus_gx_region_detect = "auto"
genesis_plus_gx_render = "single field"
genesis_plus_gx_sound_output = "stereo"
genesis_plus_gx_system_hw = "auto"
genesis_plus_gx_ym2413 = "enabled"
genesis_plus_gx_ym2413_core = "mame"
genesis_plus_gx_ym2612 = "mame (asic ym3438)"
```