---
title: Sony Playstation
description: What the muOS CFW currently uses to run Sony Playstation ROMs.
hide:
  - toc
---

One of the more heavier systems to work on the RG35XX which is truely amazing. The file `PSXONPSP660.bin` will need to be placed into the `BIOS` folder for this core to run correctly.

You'll also need compatible games that don't require DualShock controllers, you can find a list from the following handy Reddit link.

[https://old.reddit.com/r/RetroPie/comments/87ajca/psx_games_that_require_l2r2/dwd4moq/](https://old.reddit.com/r/RetroPie/comments/87ajca/psx_games_that_require_l2r2/dwd4moq/)

If you do decide to get **ALL** of the Playstation ROMs you'll probably need a hefty 512GB or even 1TB SD Card.

## Configuration

The following should be saved as `PCSX-ReARMed.opt` in the same folder name under `.retroarch/config`.

```
pcsx_rearmed_analog_axis_modifier = "circle"
pcsx_rearmed_bios = "auto"
pcsx_rearmed_display_internal_fps = "disabled"
pcsx_rearmed_dithering = "disabled"
pcsx_rearmed_drc = "enabled"
pcsx_rearmed_duping_enable = "disabled"
pcsx_rearmed_frameskip_interval = "3"
pcsx_rearmed_frameskip_threshold = "33"
pcsx_rearmed_frameskip_type = "disabled"
pcsx_rearmed_gpu_slow_llists = "auto"
pcsx_rearmed_gteregsunneeded = "enabled"
pcsx_rearmed_gunconadjustratiox = "1.00"
pcsx_rearmed_gunconadjustratioy = "1.00"
pcsx_rearmed_gunconadjustx = "0"
pcsx_rearmed_gunconadjusty = "0"
pcsx_rearmed_icache_emulation = "disabled"
pcsx_rearmed_input_sensitivity = "1.00"
pcsx_rearmed_memcard2 = "disabled"
pcsx_rearmed_multitap = "disabled"
pcsx_rearmed_negcon_deadzone = "0"
pcsx_rearmed_negcon_response = "linear"
pcsx_rearmed_neon_enhancement_enable = "enabled"
pcsx_rearmed_neon_enhancement_no_main = "disabled"
pcsx_rearmed_neon_interlace_enable = "disabled"
pcsx_rearmed_nocdaudio = "enabled"
pcsx_rearmed_nocompathacks = "disabled"
pcsx_rearmed_nogteflags = "enabled"
pcsx_rearmed_nosmccheck = "disabled"
pcsx_rearmed_nostalls = "enabled"
pcsx_rearmed_noxadecoding = "enabled"
pcsx_rearmed_psxclock = "82"
pcsx_rearmed_region = "auto"
pcsx_rearmed_show_bios_bootlogo = "disabled"
pcsx_rearmed_show_input_settings = "disabled"
pcsx_rearmed_spu_interpolation = "simple"
pcsx_rearmed_spu_reverb = "enabled"
pcsx_rearmed_vibration = "enabled"
```