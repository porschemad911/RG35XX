---
title: Wolfenstein 3D
description: What the muOS CFW currently uses to run Wolfenstein 3D ROMs.
hide:
  - toc
---

Can't go past one of the original FPS games. That's right Super 3D Noah's Ark! Also Wolfenstein and Spear of Destiny too I guess.

You'll need `ecwolf.pk3` in the `BIOS` folder for it to run.

There are a couple of issues with the core at the moment, one of them being that it seems to randomly crash whilst shooting. The other is that Spear of Destiny may not load properly if at all.

## Configuration

The following should be saved as `ecwolf.opt` in the same folder name under `.retroarch/config`.

```
ecwolf-adlib-volume = "20"
ecwolf-alwaysrun = "disabled"
ecwolf-am-drawtexturedfloors = "disabled"
ecwolf-am-drawtexturedwalls = "enabled"
ecwolf-am-overlay = "off"
ecwolf-am-pause = "enabled"
ecwolf-am-rotate = "off"
ecwolf-am-showratios = "enabled"
ecwolf-am-texturedoverlay = "disabled"
ecwolf-analog-deadzone = "15%"
ecwolf-analog-move-sensitivity = "20"
ecwolf-analog-turn-sensitivity = "20"
ecwolf-aspect = "auto"
ecwolf-digi-volume = "20"
ecwolf-dynamic-fps = "disabled"
ecwolf-effects-priority = "digi-adlib-speaker"
ecwolf-fps = "35"
ecwolf-invulnerability = "disabled"
ecwolf-memstore = "disabled"
ecwolf-music-volume = "15"
ecwolf-palette = "rgb565"
ecwolf-panx-adjustment = "5"
ecwolf-pany-adjustment = "5"
ecwolf-preload-digisounds = "disabled"
ecwolf-resolution = "320x200"
ecwolf-speaker-volume = "20"
ecwolf-viewsize = "20"
```