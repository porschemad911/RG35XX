---
title: Building RetroArch
description: How to build the custom RetroArch executable using Buildroot and various patches.
---

muOS uses the Buildroot method of creating dependencies that will compile RetroArch and its cores.

Download Buildroot version 2023.02 and extract it to your home directory. Then run the following.

```bash
cd buildroot-2023.02

wget -O .config https://codeberg.org/adixal/RG35XX/src/branch/main/dev/muOS-2305r-brc.config

./utils/brmake -j$((`nproc`))
```

This may take quite a while depending on your CPU. Fingers crossed that everything compiles successfully and you have no errors.

## Compiling RetroArch

Follow the steps below to compile RetroArch if your Buildroot compiles successfully. This is a huge work in progress but is quite stable and usable in its current state.

```bash
wget https://codeberg.org/adixal/RG35XX/raw/branch/main/dev/crosscompile.sh
. ./crosscompile.sh

git clone https://github.com/libretro/RetroArch
cd RetroArch

wget https://codeberg.org/adixal/RG35XX/raw/branch/main/dev/muOS-2305r.patch
git apply muOS-2305r.patch

wget https://codeberg.org/adixal/RG35XX/raw/branch/main/dev/Makefile.RG35XX
./configure --host=$XHOST

make clean
make -j$((`nproc`)) -f Makefile.RG35XX
```

You can now copy the compiled `retroarch` file to your device in the appropriate directory where RetroArch resides. Make a backup copy of your existing one first!