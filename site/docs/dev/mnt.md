---
title: Mounting and Modifying
description: How to mount the muOS CFW image to make custom modifications.
---

This is technically for Linux only, you may be able to do the following steps with MacOS or WSL2 but I have not tried it. Sorry, feel free to reach out to me if you're able to do this and I'll adjust this page!

You'll need to have loopback filesystem support. Most distributions will have that as a standard feature. The other thing you'll need is essential utilities installed from the `util-linux` suite.

## Mounting
Now this is just for mounting the raw `IMG` file you may see with the StockOS or other systems. The first section will find out what loopback device is available and then mount the image to that device. You may also have some luck in using GUI applications such as `gnome-disks` to mount the image, but everybody loves the terminal right?

```bash
sudo losetup -f

# The next line is simply the output which you'll use for the next line.
# > /dev/loop0
# Remember the loopback device!

sudo losetup /dev/loop0 ~/yourfile.img
```

Then we can probe all of the partitions that are available to us inside the image. It should also mount correctly into your directory manager, i.e. Nautilus, Dolphin, PCManFM, Thunar.

```bash
sudo partprobe /dev/loop0
```

## Modifying
You should now be able to modify the filesystem by adding or removing files as you need to, if you run into trouble you can also use the `chown` and `chmod` tools to change access. I won't go into details on how to use those utilities as they shouldn't be required.

You can also install and launch `gparted` to adjust partition sizes or even shrink the image if you're careful enough! Just use your favourite package manager to do so. Guess what mine is!

```bash
sudo dnf install gparted f2fs-tools hfsutils jfsutils exfatprogs
sudo gparted /dev/loop0
```

## Finishing
Once you're done with the image, eject it as you would normally a USB or SD card. Then run the following commands.

```bash
sudo losetup -d /dev/loop0
```