---
title: muOS Extras
description: Explanation of every single function that muOS Extras has to offer.
hide:
  - navigation
  - toc
---

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![Main Screen](assets/extra/0.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    The main screen that also shows what SD Cards are currently mounted.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![System Date and Time](assets/extra/1.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    Set the system date and time so certain ROMs can take advantage.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![Utilities](assets/extra/2.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    Extra utilities and functions! Most of these are explained in other screenshots.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![Activity Tracker](assets/extra/4.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    View the top games that you have played, this interprets the RetroArch play activity.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![Extraction](assets/extra/5.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    Extract archives to enhance the functionality of muOS and RetroArch.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![SD Card Tools](assets/extra/7.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    Various tools that can be used to help format, sync, or run health checks on SD Cards.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![System Information](assets/extra/8.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    Display the current system information including uptime, memory, and system temperature.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![Boot Options](assets/extra/9.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    Change the different boot options including themes, battery type, and debugging.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![System Timers](assets/extra/10.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    Adjust the timers for system idle, shutdown, and the low battery indicator.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![RetroArch / System Reset](assets/extra/11.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;text-align:center;">
    Allows you to reset the configuration of RetroArch or factory reset the device.
  </p>
</div>

<div style="width:320px;float:left;margin-right:2rem;" markdown>
  ![RetroArch Backups](assets/extra/12.png){ width="320" }
  <p style="font-size:0.65rem;margin-top:-1rem;margin-bottom:2rem;text-align:center;">
    Create a backup of all your configurations, playlists, and save games.
  </p>
</div>