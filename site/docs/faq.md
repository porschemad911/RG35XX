---
title: Features
description: List of core features muOS has to offer.
---

Wondering what the core features of muOS are? All of the below information is correct as of release 2305r.

## System Features

#### Partition and general image modification
  * Removed unnecessary partitions from image and files in SYSTEM partition.
  * Hidden SYSTEM partition due to Windows being annoying about EXT4 partitions.
  * Custom icons and drive labels for Windows systems.

#### Simplified image process
  * No need to use a partition manager to expand the last partition
  * Write the image to an SD Card, insert into RG35XX, reboot.
  * The device will go through a factory reset process with a progress bar.
  * Also has nice music to listen to while you wait.

#### Simplified boot options
  * Now uses `options.txt` file in `BOOT` drive for extra functionality.
  * This simplified file reduces risk of errors.
  * Self-explanatory comments inside the file.

#### Modified ramdisk
  * Reduce boot overhead and boot time by ~2 seconds.
  * Includes changes to internal scripts.
  * Ability to have custom charging indicators _(images can be change on `BOOT` drive)_.

#### muOS Extras application
  * This allows easy to configure boot options.
  * No longer require to modify the `options.txt` file directly.
  * Adjust the date and time.
  * Backup and reset configuration and the entire system.

#### Easy-to-use updating system
  * Place the `update.7z` file into `BOOT` drive to update.
  * Reboot the device to start the updating process.
  * Progress bar will indicate where it is at.

#### Low battery indicator
  * Flashes screen by default.
  * Works based on capacity level.

#### Boot animation and chime support
  * Look at `anim` folder on the `BOOT` drive for more information.
  * By default muOS will boot with 6 different chimes.

#### Brightness background system process
  * _(JoyBright)_ which can go down pretty low.
  * Use `SELECT + VOL UP/DOWN` to change brightness.
  * This is not restricted to RetroArch, works for the entire system.
  * Screen will switch off on 0% brightness.
  * Brightness will be restored on device restart.

#### Audio background system process
  * _(JoyAudio)_ which switches the speaker off not just keeps it low.
  * Use `SELECT + VOL UP/DOWN` to change volume.
  * Use `SELECT + DPAD DOWN` to mute. Adjusting the volume will unmute.
  * Volume changes are not restricted to RetroArch, works for the entire system.
  * Speaker will switch off at 0% volume.
  * Volume will be restored on device restart.

#### Idle input background system process
  * _(JoyIdle)_ sleep mode and shutdown procedure.
  * Will switch the screen off and mute the speaker after 5 minutes.
  * Automatically switch the device off after 10 minutes.
  * Timing can be changed through `options.txt` file on the BOOT drive.

## RetroArch Features

#### CPU Frequency and Governor selection for cores
  * Currently set to `userspace` for cores and `powersave` for the menu.
  * Maximum CPU is set to `1388MHz` for stability.

#### Multiple changes to RGUI
  * Removed unneccescary options to make it more clean.
  * Fullscreen images as thumbnails.

#### Native RetroArch feature support
  * ROM searching and filtering.
  * Save search results to a specific view which can be accessed through playlists.
  * ROMs and BIOS files can be placed anywhere thanks to RetroArch custom directory support.

#### New features for RetroArch
  * Shuffle mode to reduce choice paralysis.
  * Full PORT game and application support through Libretro Port core.
  * Idle suspend screensaver _(starfield as default)_.

## Coming Soon
  * Improved built-in video and audio filters.
  * Native media players.

## Disadvantages
  * No bilinear video interpolation.
  * Overlay images are not working, unsure as to why.
  * HDMI support has currently been disabled until further notice.
  * ROMs need to be scanned in manually.